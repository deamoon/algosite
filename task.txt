User feedback:
  use model file -> upload files on server
  use corellation
  change min hold
  close next period, not next close
  bug with enter
  limit personal server limit and min hold, etc.

Don't forget:
  Competition data date start in 1 september
  Change rating formula
  Retest backtestAttempt also
  Change order fundtion in backtest ipynb and rewrite table show mode to procent or add warning
  onlineTicker add null

Errors:
  check error line if holdperiod is not int
  rename sumProcent in Attempt to avgProcent

Problem:
  P&L don't finished, TODO in result.html
  https://bcsquants.com/attempt/1083/

Task:
  Errors check how looks, correct line, special error with -
  Optimize download data and check that data downloaded not full

Architecture:
  Logs
  Unittest + test of production working
  Monitoring - monitoring disk space, cpu, memory, celery task, cron, logs
  CI
  Benchmark
  Asnsilbe

Done:
  ok, Dns domain
  ok, time of data server update, через полчаса можно качать (quik online, bloomberg online but 15 min delay and 1 min for update)
  ok, backtest with Dima, check all rules
  ok, think about advert
  ok, check progress of frontend
  ok, update documatnation with dima robots and rules
  ok, Add hdd to cloudlite
  ok, backtest without ip
  ok, ssl
  ok, User rating
  ok, Don't work callback after attempt finished
  ok, frontend integration
  ok, Documentation
  Open source backtest
  Limit - limit run, time, cpu, memory, disk space
  Security - security run
  Tutorial in documentation
  Block button send online until choose instrument
  Move position from test to control don't calculate properly, affect buy and hold starategy. Need to check not only dateEnter. Should close position at the end of period and open the same at the start of the next period
  https://bcsquants.com/attempt/928/ -> ALRS 2nd order takeProfit == 0.04 but real 0.06

План:
  Тестирование, внутреннее
  Задачи:
    Frontend
    Тестирование
    Лимиты и безопасность

  Реклама
    Группа ВК:
      Начальные материалы
    Материалы для блога
    Как рекламироваться:
      Объявление вузы:
        Делаем объявление
      Таргет вк
  Обучение
  Blog, news, feedback
  Formula rating(/avgNet), strategy(best 3), rating user(sum of online?)

1. Window:
  окно расширяемое, в онлайне и backtest по разному
2. мы не закрываем в конце дня
3. нет выходных и только рабочие часы, 10:00-18:40
4. холд указывается в свечках, холд не инвариантен относительно размера свечки
5. холд по данным, не по календарным часам
6. минимальный холд 300 секунд, с ошибкой, если не так
7. минимальный профит или комиссия 0.001% (поднять?)
8. закрываем по цене, не фиксированы проценты
9. тригеры на high и low, а закрываем по close
