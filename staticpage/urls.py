from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns(
    '',
    url(r'^about/$', TemplateView.as_view(template_name='staticpage/about.html'), name="about"),
    url(r'^div/$', 'staticpage.views.lets_divide'),
)
