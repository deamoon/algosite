var name, phone, mail,client_surname,consultation, mail_server, phone_server, name_server = '';


function replace_text_in_form(form) {

	var $title = $(form).parents('article').find('.page-form-title');
	$title.find('.main-title').slideUp(500);
	$title.find('.second-title').slideDown(500);

	return false;

}

jQuery(function($) {

	// Fix fixed bg's jump
	/MSIE [6-8]|Mac/i.test(navigator.userAgent)||$("header, article, footer").each(function(){if("fixed"==$(this).css("backgroundAttachment")){var i=$(this),a=/WebKit/i.test(navigator.userAgent)?9:8;i.addClass("froid-fixed-bg").data({bgX:i.css("backgroundPosition").slice(0,i.css("backgroundPosition").indexOf(" ")),bgY:i.css("backgroundPosition").slice(i.css("backgroundPosition").indexOf(" ")),margin:a})}}),$(window).bind("SIModals.modalsOpen",function(){$(".froid-fixed-bg").each(function(){var i=$(this);i.css("backgroundPosition","calc("+i.data("bgX")+" - "+i.data("margin")+"px) "+i.data("bgY"))})}),$(window).bind("SIModals.modalsClose",function(){$(".froid-fixed-bg").each(function(){var i=$(this);i.css("backgroundPosition",i.data("bgX")+" "+i.data("bgY"))})});

	// Mobile full-width && disable animation
	if(is_mobile()) {

		// Fix mobile fixed bg's
		$("header, article, footer").each(function(){if ("fixed" == $(this).css("backgroundAttachment")) $(this).css('backgroundAttachment', 'scroll');});

		// Mobile stretch
		$('html, body').addClass('mobile');
		//$('html').css('width', window.innerWidth + 'px');

		// Remove animation
		$('.cre-animate').css({'transform': 'none', '-webkit-transform': 'none', '-moz-transform': 'none', '-ms-transform': 'none', '-o-transform': 'none', 'transition': 'none', '-webkit-transition': 'none', 'opacity' : 1}).removeClass('.cre-animate');

	}

	if (is_OSX()) {
		$('html, body').addClass('osx');
	}

	// Init all plugins and scripts
	$.fn.SIInit = function() {

		// Modal photos
		$('a[data-rel]').each(function() {$(this).attr('rel', $(this).data('rel'));});
		$('[data-href]').each(function() {$(this).attr('href', $(this).data('href'));});
		$('a[rel^=fancybox]').not('.cloned a').fancybox({
			helpers : {
				thumbs : true
			}
		});
		// Forms
		$('.send-form').SIForms({
			'validateFields': {
				'client_name' 		: 'Укажите Ваше имя',
				'client_phone' 		: 'Укажите Ваш телефон',
				'client_mail' 		: 'Укажите Ваш e-mail',
				'client_surname' 	: 'Укажите Вашу фамилию',
				'client_birthday' 	: 'Укажите дату рождения',
				'client_city' 		: 'Укажите Ваш город',
			},

			'checkExtra' : function(form) {

				if ($(form).parents('article').find('.form-agree').size() > 0) {

					if (!$(form).parents('article').find('.form-agree:visible').find('.form-agree-check').hasClass('checked')) {
						SIPageMessages.show('Пожалуйста, примите условия пользовательского соглашения и попробуйте снова. Спасибо!', 3500);
						return false;
					}

				}

				if($(form).hasClass("step-1") || consultation) {
					name = $(form).find('.client-name').val();
					phone = $(form).find('.client-phone').val();
					mail = $(form).find('.client-mail').val();
					$.ajax({
						type: "POST",
						url: "/customer/registration/validation-step-one",
						async: false,
						dataType: 'json',
						data: {email: mail, mobile_phone: phone, first_name_ru: name}
					}).success(function (data) {
						mail_server = data.email;
						phone_server = data.mobile_phone;
						name_server = data.first_name_ru;
					});

					if (name_server) {
						$(form).find('.client-name').addClass('si-error');
					}
					if (mail_server) {
						$(form).find('.client-mail').addClass('si-error');
					}
					if (phone_server) {
						$(form).find('.client-phone').addClass('si-error');
					}
					if (name_server || mail_server || phone_server) {
						return false;
					}
				}

				if($(form).hasClass("consultation")) {
					name = $(form).find('.client-name').val();
					phone = $(form).find('.client-phone').val();
					mail = $(form).find('.client-mail').val();

					var patternMail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
					var patternName =/^[А-Яа-яЁё\-\ ]+$/i;

					if (name == '' || !patternName.test(name)) {
						$(form).find('.client-name').addClass('si-error');
					}
					if (mail== '' || !patternMail.test(mail)) {
						$(form).find('.client-mail').addClass('si-error');
					}
					if (phone == '') {
						$(form).find('.client-phone ').addClass('si-error');
					}
					if ((name == '') || (phone == '') || (mail == '') || (!patternMail.test(mail)) || (!patternName.test(name))) {
						return false;
					}
				}

			},

			'sendSuccess' : function(res, form) {

					if ($(form).hasClass('page-form step-1')) {

						setTimeout(function () {

							$("html, body").animate({scrollTop: $(form).offset().top - 100}, 1000);

							var $parent = $(form).parents('article');

							$(form).parents('article').find('.for-pc').css('visibility', 'hidden');
							$parent.find('.page-form.step-1').hide();
							$parent.find('.page-form.step-2').show();

							$parent.find('.page-form.step-2 .client-name').val(name);
							$parent.find('.page-form.step-2 .client-phone').val(phone);
							$parent.find('.page-form.step-2 .client-mail').val(mail);

							$parent.find('.page-form-step').removeClass('active');
							$parent.find('.page-form-step.step-2').addClass('active');

							$('select').trigger('refresh');

							$(form).find('.client-surname').focus();
							$(form).find('.form-agree').hide();

						}, 1000);


					}
				}

				/*
				yaCounter.reachGoal('target' + res.id);

				ga('send', 'event', res.gcode, res.gcode);
				*/


		});





	};

	$.fn.SIInit();

	// Styler
	$('select').styler();
	$(".client-birthday").mask("99.99.9999",{placeholder:"дд/мм/гггг"});

	// Jump links
	$('.si-jump').SIJump();

	// Page messages
	SIPageMessages.init();

	// Agree
	$('.form-agree').click(function(e){
		$(this).find('.form-agree-check').toggleClass('checked');
		e.preventDefault();
	});

		// Link in agree
		$('.form-agree a').click(function(e){
			$('.open-privacy-modal').last().click();
			e.preventDefault();
			e.stopPropagation();
		})

	// Conditions

		var mh = 0;
		$('.condition-item').each(function(){
			mh = Math.max($(this).outerHeight(), mh);
		});
		$('.condition-item').animate({'min-height' : mh}, 500);


	// Select

		$('.select-title').click(function() {

			if ($('.select-item').css('display') != 'block') return false;

			var $parent = $(this).parent();
			var $text = $parent.find('.select-text');

			if ($text.is(':hidden')) {
				$text.slideDown(500);
				$(this).addClass('active');
			}else{
				$text.slideUp(500);
				$(this).removeClass('active');
			}

		})

	// About

		var mh = 0;
		$('.about-item').each(function(){
			mh = Math.max($(this).height(), mh);
		});
		$('.about-item').animate({'min-height' : mh}, 500);

	// Awards

		// Slider
		$('.awards').owlCarousel({loop:false,items:5,margin:0,dots:false,
			responsive : {

				0 : {

					loop:true,items:1,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				600 : {

					loop:true,items:2,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				960 : {

					loop:true,items:3,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				1150 : {
					loop:false,items:5,margin:0,nav:false,mouseDrag:false,touchDrag:false,pullDrag:false,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']
				},

			}

		});

	// Certs

		// Slider
		$('.certs').owlCarousel({loop:false,items:5,margin:0,dots:false,
			responsive : {

				0 : {

					loop:true,items:1,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				600 : {

					loop:true,items:2,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				960 : {

					loop:true,items:3,margin:0,nav:true,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']

				},

				1150 : {
					loop:false,items:5,margin:0,nav:false,mouseDrag:false,touchDrag:false,pullDrag:false,
					navText:['<span class="si-arrow-left"></span><span class="si-arrow-left hovered"></span>', '<span class="si-arrow-right"></span><span class="si-arrow-right hovered"></span>']
				},

			}

		});


	// Modals
	SIModals.init();

		// Init modals
		SIModals.attachModal('.open-phone-modal', '.phone-modal', {'.send-extra' : 'extra'});
		SIModals.attachModal('.open-privacy-modal', '.privacy-modal', {'.send-extra' : 'extra'});

		// Modal controls
		SIModals.attachClose('.si-close');
		SIModals.attachClose('.si-close2');

});




var markers = [];
var routing = false;
var contactMap;

function hoverMarker(num) {
    var marker = markers[num];
    var placemark = marker.placemark;
    placemark.options.set('iconContentLayout', ymaps.templateLayoutFactory.createClass('<span class="marker active">'+num+'</span>'));
    $('.point[data-point='+num+'] .marker').addClass('hovered');
}

function blurMarker(num) {
    var marker = markers[num];
    var placemark = marker.placemark;
    if (!marker.active)
        placemark.options.set('iconContentLayout', ymaps.templateLayoutFactory.createClass('<span class="marker">'+num+'</span>'));
    $('.point[data-point='+num+'] .marker').removeClass('hovered');
}

function openRoute() {
    closeAllPointDetails();
    $('.route').show();
    bindTogglebox();
    routing = true;
}

function closeRoute() {
    removeRoute();
    $('.route').hide();
    routing = false;
}

function clearRoutes() {
    $('.route .route-points input').val('');
}
function setRoutePoint(coord, name, n) {
    if (n == 1)
    {
        $('#pointA').val(name);
        $('#pointA-coord').val(coord);
    }
    else
    {
        $('#pointB').val(name);
        $('#pointB-coord').val(coord);
    }

    if ($('#pointA-coord').val().length && $('#pointB-coord').val().length)
    {
        setRoute();
    }
}
var multiRoute=false;
function removeRoute() {
    if (multiRoute) {
        contactMap.geoObjects.remove(multiRoute);
    }
    multiRoute = false;
}

function setRoute() {
    removeRoute();
    var mode = $('#routingMode').val();
    var avoidTraffic = $('#avoidTraffic').is(':checked');
    var pointA = ($('#pointA-coord').val().length) ? $('#pointA-coord').val() : $('#pointA').val();
    var pointB = ($('#pointB-coord').val().length) ? $('#pointB-coord').val() : $('#pointB').val();

    multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: [
            pointA,
            pointB
        ],
        params: {
            routingMode: mode,
            avoidTrafficJams: avoidTraffic
        }
    }, {
        routeStrokeWidth: 1,
        routeActiveStrokeWidth: 4,
        routeStrokeColor: "#ffffff",
        routeActiveStrokeColor: "#122f5b",
        wayPointStartIconLayout: "default#image",
        wayPointStartIconImageHref: '/content/images/map-marker.png',
        wayPointStartContentSize: [10, 10],
        wayPointStartIconImageSize: [27, 36],
        wayPointStartIconImageOffset: [-13, -36],
        wayPointFinishIconLayout: "default#image",
        wayPointFinishIconImageHref: '/content/images/map-marker.png',
        wayPointFinishContentSize: [10, 10],
        wayPointFinishIconImageSize: [27, 36],
        wayPointFinishIconImageOffset: [-13, -36]
    });
    contactMap.geoObjects.add(multiRoute);

    /*ymaps.route([
        $('#pointA-coord').val(),
        $('#pointB-coord').val()
    ], {
        routingMode: mode,
        avoidTrafficJams: avoidTraffic,
        multiRoute: true
    }).then(function (route) {
            contactMap.geoObjects.add(route);
        });*/
}

function openPointDetails(num) {
    if ($('.point-detail[data-point='+num+']').is(':visible'))
        return false;

    removeRoute();
    closeAllPointDetails();
    $('.points').hide();
    closeRoute();
    $('.point-detail[data-point='+num+']').show();

    var marker = markers[num];
    marker.active = true;
    markers[num] = marker;
    hoverMarker(num);
}
function closePointDetails(num) {
    $('.point-detail[data-point='+num+']').hide();
    var marker = markers[num];
    marker.active = false;
    markers[num] = marker;
    blurMarker(num);
}

function closeAllPointDetails() {

	$('.point-detail:visible').each(function(){
        var num = parseInt($(this).attr('data-point'));
        closePointDetails(num);
    });
}

$(function () {
	$('.point-detail .close').click(function () {
		var point = $(this).parent();
		var num = parseInt(point.attr('data-point'));
		closePointDetails(num);
		$('.points').show();
		return false;
	});

	$('.route .close').on('click', function () {
		$('.points').show();
		closeRoute();
		return false;
	});

	$('.contact-map .points a').on('mouseenter', function () {
		var num = parseInt($(this).attr('data-point'));
		hoverMarker(num);
	});
	$('.contact-map .points a').on('mouseleave', function () {
		var num = parseInt($(this).attr('data-point'));
		blurMarker(num);
	});
	$('.contact-map .points a').on('click', function () {
		var num = parseInt($(this).attr('data-point'), 10);
		openPointDetails(num);
		return false;
	});

	if ($('#contact-map') && $('#contact-map').length) {
		ymaps.ready(function () {
			contactMap = new ymaps.Map("contact-map",
                {
                	center: [center_latitude, center_longitude],
                	zoom: 11,
                	controls: [],
                	behaviors: ['drag']
                }
            );

			contactMap.events.add('click', function (e) {
				var coords = e.get('coords');
				if (routing) {
					ymaps.geocode(coords, {
						kind: 'house',
						results: 1
					}).then(function (res) {
						var firstGeoObject = res.geoObjects.get(0);
						var name = firstGeoObject.properties.get('name');
						//var text = firstGeoObject.properties.get('text');

						if (!$('#pointA').val().length)
							setRoutePoint(coords, name, 1);
						else if (!$('#pointB').val().length)
							setRoutePoint(coords, name, 2);
					});
				}
			});

			for (var j in contactPoints) {
				var point = contactPoints[j];
				var placemark = new ymaps.Placemark(point, {}, {
					iconLayout: 'default#imageWithContent',
					iconImageHref: '',
					iconImageSize: [44, 44],
					iconImageOffset: [-22, -22],
					iconContentSize: [44, 44],
					pointNum: j,
					iconContentLayout: ymaps.templateLayoutFactory.createClass('<span class="marker">' + j + '</span>')
				});
				placemark.events
                .add('mouseenter', function (e) {
                	var num = e.get('target').options.get('pointNum');
                	hoverMarker(num);
                })
                .add('mouseleave', function (e) {
                	var num = e.get('target').options.get('pointNum');
                	blurMarker(num);
                })
                .add('click', function (e) {
                	var num = e.get('target').options.get('pointNum');
                	openPointDetails(num);
                });
				contactMap.geoObjects.add(placemark);
				markers[j] = {
					placemark: placemark,
					active: false
				};
			}

			$('#zoom-in').click(function () {
				var zoom = contactMap.getZoom();
				if (zoom >= 17)
					return false;
				contactMap.setZoom(zoom + 1, { duration: 200 });
			});
			$('#zoom-out').click(function () {
				var zoom = contactMap.getZoom();
				if (zoom <= 1)
					return false;
				contactMap.setZoom(zoom - 1, { duration: 200 });
			});
		});
	}

});