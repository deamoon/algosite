// scripts only for demonstrate page-proofs
$(document).ready(function() {

  var HIDDEN_CLASS = 'h-hidden';
  var ACTIVE_CLASS = '-active';

  $(document).on('click', '.js-tab', function(e) {
    var $this = $(e.currentTarget);
    var tab = $this.data('tab');

    if($this.hasClass(ACTIVE_CLASS)) return;

    $('.js-tab').removeClass(ACTIVE_CLASS);
    $this.addClass(ACTIVE_CLASS);
    $('.js-tab-content').addClass(HIDDEN_CLASS);
    $(tab).removeClass(HIDDEN_CLASS);

  });

  $('.modal-trigger').leanModal();

  $('select').material_select();

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 100 // Creates a dropdown of 15 years to control year
  });
});
