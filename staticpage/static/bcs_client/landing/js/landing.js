var frm = $('#email-form');
frm.submit(function () {
    var wait_text = $('#form-button').attr('data-wait')
    var reg_text = $('#form-button').attr('default')

    $("#form-message").empty()
    $("#form-error").empty()

    $('#form-button').attr('disabled', 'disabled')
    $('#form-button').attr('value', wait_text)

    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            if (data['error']) {
                $("#form-error").html(data['error']);
            } else {
                $("#form-message").html(data['message']);
            }
        },
        error: function(data) {
            $("#form-error").html('Ошибка отправки email');
        },
        complete: function(data) {
            $('#form-button').attr('value', reg_text)
            $("#form-button").removeAttr('disabled');
        }
    });
    return false;
});