from django.db import models

class Candle(models.Model):
    open = models.DecimalField(max_digits=12, decimal_places=6)
    high = models.DecimalField(max_digits=12, decimal_places=6)
    low = models.DecimalField(max_digits=12, decimal_places=6)
    close = models.DecimalField(max_digits=12, decimal_places=6)
    volume = models.BigIntegerField()
    number = models.BigIntegerField()

    SIZE_LIST = (
        ('1s', '1 second'),
        ('5s', '5 seconds'),
        ('1m', '1 minute'),
        ('5m', '5 minutes'),
    )

    start = models.DateTimeField()
    end = models.DateTimeField()
    size = models.CharField(max_length=20, choices=SIZE_LIST)

    ticker = models.CharField(max_length=30)

    class Meta:
        index_together = (
            ('ticker', 'size', 'start'),
        )
        unique_together = (
            ('ticker', 'size', 'start'),
        )