#coding=utf-8

import sys, os, django
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ['DJANGO_SETTINGS_MODULE'] = 'algosite.settings'
django.setup()

from algosite.settings import ALGOSITE_DATABOX
import os
import requests
import datetime
from datetime import datetime as dt
import numpy
from subprocess import call
from bcs_client.models import AttemptBcs, Worker, Profile, StrategyBcs
import re

def createPath(path):
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

DATA_SERVER = 'http://89.249.27.203:50011'

def getOpenDateList(instrument):
    urlFormat = DATA_SERVER + '/papers/{provider}/{board}/{symbol}'
    url = urlFormat.format(**instrument)
    r = requests.get(url)
    result = r.text.strip()
    dateStrList = re.findall(r'<Day>(.*?)</Day>', result)
    dateList = [dt.strptime(d, '%Y.%m.%d').date() for d in dateStrList]
    return dateList

def getFileName(universary, dataOptions):
    createPath(os.path.join(ALGOSITE_DATABOX))
    createPath(os.path.join(ALGOSITE_DATABOX, universary['name']))
    createPath(os.path.join(ALGOSITE_DATABOX, universary['name'], dataOptions['tickSize']))
    filePath = os.path.join(ALGOSITE_DATABOX, universary['name'], dataOptions['tickSize'], dataOptions['symbol'])
    createPath(filePath)
    fileName = os.path.join(filePath, '{0}.csv'.format(dataOptions['date']))
    return fileName

def deleteCloseDateFile(universary, dataOptions):
    fileName = getFileName(universary, dataOptions)
    if os.path.isfile(fileName):
        os.remove(fileName)

def downloadData(universary, dataOptions, isRewrite=False):
    dataFormat = DATA_SERVER + '/papers/{provider}/{board}/{symbol}/{date}/1000-1840/{tickSize}/csv'
    fileName = getFileName(universary, dataOptions)

    if os.path.isfile(fileName) and not isRewrite:
        with open(fileName, 'r') as file:
            if len(file.readlines()) < 3:
                deleteCloseDateFile(universary, dataOptions)
            else:
                return

    dataUrl = dataFormat.format(**dataOptions)
    r = requests.get(dataUrl)
    result = r.text
    if result[:4] != 'Time':
        raise RuntimeError('Wrong data url {url}'.format(url=dataUrl))
    dataResult = result.replace('\r', '')

    with open(fileName, 'w') as file:
        file.write(dataResult)

def downloadAllData(universary, showProgress=False):
    dateStart = dt.strptime(universary['firstTestDatetime'], "%Y-%m-%d %H:%M:%S")
    now = dt.now()

    progress = 0
    numberDays = len(universary['instruments']) * len(universary['tickSizes']) * ((now - dateStart).days + 1)

    if showProgress:
        print('Start downloadAllData numberDays = {0}'.format(numberDays))

    delta = datetime.timedelta(days=1)
    for instrument in universary['instruments']:
        d = dateStart
        openDates = set(getOpenDateList(instrument))
        dataOptions = instrument.copy()
        while d <= now:
            date = d.strftime("%Y%m%d")
            dataOptions['date'] = date
            for tickSize in universary['tickSizes']:
                dataOptions['tickSize'] = tickSize

                isOpenDate = d.date() in openDates
                if isOpenDate:
                    downloadData(universary, dataOptions
                                # isRewrite=True
                                )
                else:
                    deleteCloseDateFile(universary, dataOptions)

                progress += 1

                if showProgress:
                    if progress % 10 == 0:
                        print('{0} / {1}, {2} {3} Done'.format(progress, numberDays, instrument, tickSize))

            d += delta

def convertDataInNumpy(universary, csvFolder, tickSize, instrument):
    fileNames = [fileName for fileName in os.listdir(csvFolder)]
    fileNames.sort()

    dataArray = [[] for _ in universary['keysData']]
    for fileName in fileNames:
        with open(os.path.join(csvFolder, fileName)) as file:
            sumCount = 0
            content = file.readlines()
            for line in content[1:]:
                sumCount += int(line.split(';')[-2])
            if sumCount != 0:
                for line in content[1:]:
                    for ind, item in enumerate(line.split(';')[:-1]):
                        dataArray[ind].append(universary['keysFunction'][ind](item))

    # startTime = fileNames[0].split('.')[0]
    # endTime = fileNames[-1].split('.')[0]
    # prefix = tickSize + '_' + instrument + '_' + startTime + '_' + endTime + '_'

    createPath(os.path.join(ALGOSITE_DATABOX, universary['name']))
    path = os.path.join(ALGOSITE_DATABOX, universary['name'], 'syncData')
    createPath(path)
    for ind, data in enumerate(dataArray):
        createPath(os.path.join(path, tickSize))
        createPath(os.path.join(path, tickSize, instrument))
        pathFile = os.path.join(path, tickSize, instrument, universary['keysData'][ind])
        numpy.save(pathFile, data)
        # print('Save {0}'.format(pathFile))


def convertAllDataInNumpy(universary):
    for tickSize in universary['tickSizes']:
        path = os.path.join(ALGOSITE_DATABOX, universary['name'], tickSize)
        for instrument in os.listdir(path):
            # print(instrument)
            convertDataInNumpy(universary, os.path.join(path, instrument), tickSize, instrument)

def syncAllData(universary):
    for backtest in universary['backtestNames']:
        resFolder = '/home/deamoon/BCS/' + universary['name']
        call("/usr/bin/rsync -azP -e 'ssh -i /home/deamoon/worker.key' syncData/ deamoon@{0}:{1}".format(
            backtest, resFolder), shell=True, cwd=os.path.join(ALGOSITE_DATABOX, universary['name']))

def updateAllOnlineAttempts():
    for attempt in AttemptBcs.objects.filter(mode='O', deleted=False):
        attempt.send()
        # attempt.backtestAttempt.send()

def updateRatingAll():
    for profile in Profile.objects.all():
        profile.updateRating()

universary4 = {
    'name': 'universary4',
    'instruments': [dict(zip([
            'symbol', 'provider', 'board'
        ], data)) for data in [
            ('ALRS', 'quik', 'TQBR'),
            ('SNGS', 'quik', 'TQBR'),
            ('MGNT', 'quik', 'TQBR'),
            ('ROSN', 'quik', 'TQBR'),
            ('MOEX', 'quik', 'TQBR'),
            ('VTBR', 'quik', 'TQBR'),
            ('LKOH', 'quik', 'TQBR'),
            ('GAZP', 'quik', 'TQBR'),
            ('SBERP', 'quik', 'TQBR'),
            ('SBER', 'quik', 'TQBR'),

            ('USD000UTSTOM', 'quik', 'CETS'),
            ('RTSI', 'quik', 'RTSIDX'),
            ('MICEXINDEXCF', 'quik', 'INDX'),

            ('GZX', 'synthetic', ''),
            ('SIX', 'synthetic', ''),
            ('BRX', 'synthetic', ''),
        ]
    ],
    'backtestNames': ['backtest1', 'backtest2', 'backtest3', 'backtest4'], # must be in /etc/hosts
    'tickSizes': ['m1', 'm5'],
    'languages': ['python3'],
    'firstTestDatetime': str(dt(2015, 1, 5, 10, 0, 0)), # 5 January 2015
    'lastTestDatetime': str(dt(2016, 8, 31, 18, 35, 0)), # 31 August 2016
    'startCompetitionDatetime': str(dt(2017, 2, 27, 10, 0, 0)), # 27 February 2017
    'endCompetitionDatetime': str(dt(2017, 5, 26, 18, 35, 0)), # 26 May 2017
    'timezone': 'Europe/Moscow',
    'keysData': ['time', 'open', 'high', 'low', 'close', 'volume', 'count'],
    'keysFunction': [lambda x: dt.strptime(x, "%d.%m.%Y %H:%M:%S"),
                     float, float, float, float, int, int],
    'dataConvertFunction': lambda x: dt.strptime(x, "%Y-%m-%d %H:%M:%S"),
    # 'downloadDataLastDay': {}
}

universary3 = {
    'name': 'universary3',
    'instruments': [dict(zip([
            'symbol', 'provider', 'board'
        ], data)) for data in [
            ('SBER', 'quik', 'TQBR'),
            ('GAZP', 'quik', 'TQBR'),
            ('RIX', 'synthetic', ''),
            ('SIX', 'synthetic', ''),
            ('BRX', 'synthetic', ''),
        ]
    ],
    'backtestNames': ['backtest1', 'backtest2'], # must be in /etc/hosts
    'tickSizes': ['s1', 's5', 'm1', 'm5'],
    'languages': ['python3'],
    'firstTestDatetime': str(dt(2016, 3, 1, 10, 0, 0)), # 1 March 2016
    'lastTestDatetime': str(dt(2016, 5, 31, 18, 35, 0)), # 31 May 2016
    'startCompetitionDatetime': str(dt(2016, 8, 1, 10, 0, 0)), # 1 August 2016
    'endCompetitionDatetime': str(dt(2016, 8, 31, 18, 40, 0)), # 31 August 2016
    'timezone': 'Europe/Moscow',
    'keysData': ['time', 'open', 'high', 'low', 'close', 'volume', 'count'],
    'keysFunction': [lambda x: dt.strptime(x, "%d.%m.%Y %H:%M:%S"),
                     float, float, float, float, int, int],
    'downloadDataLastDay': {
        'SBER' : None,
        'GAZP' : None,
        'RIX' : None,
        'SIX' : None,
        'BRX' : None,
    }
}

universary2 = {
    'name': 'universary2',
    'instruments': [dict(zip([
            'symbol', 'provider', 'board'
        ], data)) for data in [
            ('SBER', 'quik', 'TQBR'),
            ('VTBR', 'quik', 'TQBR'),
            ('LKOH', 'quik', 'TQBR'),
            ('ALRS', 'quik', 'TQBR'),
            ('MGNT', 'quik', 'TQBR'),
            ('SBERP', 'quik', 'TQBR'),
            ('ROSN', 'quik', 'TQBR'),
            ('MOEX', 'quik', 'TQBR'),
            ('GAZP', 'quik', 'TQBR'),
        ]
    ],
    'backtestNames': ['backtest1', 'backtest5'], # must be in /etc/hosts
    'tickSizes': ['s1', 's5', 'm1', 'm5'],
    'languages': ['python3'],
    'firstTestDatetime': str(dt(2015, 9, 1, 10, 0, 0)), # 1 September 2015
    'lastTestDatetime': str(dt(2016, 3, 31, 18, 40, 0)), # 31 March 2016
    'startCompetitionDatetime': str(dt(2016, 7, 17, 7, 21, 0)), # 17 July 2016
    'endCompetitionDatetime': str(dt(2016, 11, 30, 18, 40, 0)), # 30 November 2016
    'timezone': 'Europe/Moscow',
    'keysData': ['time', 'open', 'high', 'low', 'close', 'volume', 'count'],
    'keysFunction': [lambda x: dt.strptime(x, "%d.%m.%Y %H:%M:%S"),
                     float, float, float, float, int, int],
    'downloadDataLastDay': {
        'SBER' : None,
        'VTBR' : None,
        'LKOH' : None,
        'ALRS' : None,
        'MGNT' : None,
        'SBERP': None,
        'ROSN' : None,
        'MOEX' : None,
    }
}

def main():
    print(dt.now(), 'Start')

    if sys.argv[1] == 'data':
        downloadAllData(universary4)
        print(dt.now(), 'downloadAllData')

        convertAllDataInNumpy(universary4)
        print(dt.now(), 'convertAllDataInNumpy')

        syncAllData(universary4)
        print(dt.now(), 'syncAllData')

        updateAllOnlineAttempts()
        print(dt.now(), 'updateAllOnlineAttempts')

    if sys.argv[1] == 'rating':
        updateRatingAll()

    if sys.argv[1] == 'rsync':
        syncAllData(universary4)

    if sys.argv[1] == 'test':
        downloadAllData(universary4, showProgress=True)

    print(dt.now(), 'Finish')

if __name__ == '__main__':
    main()
