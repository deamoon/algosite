#coding=utf-8

from django.shortcuts import render
from django.http import JsonResponse
import datetime
from django.http import HttpResponse
from algosite.settings import ALGOSITE_DATABOX
from django.views.decorators.csrf import csrf_exempt

import os 
import requests
import datetime
from datetime import datetime as dt

@csrf_exempt
def bcs_upload(request):
    today = datetime.date.today().isoformat()

    if request.method == 'GET':

        data_request = [
            ["USD000UTSTOM", "CETS", 'QUIK'],
            # ["BRH6", "SPBFUT", 'QUIK'],
            # ["RTSI", "RTSIDX", 'QUIK'],
            # ["GDH6", "SPBFUT", 'QUIK'],
            # ["MICEX", "RTSIDX", 'QUIK'],
            # ["SIH6", "SPBFUT", 'QUIK'],

            # ["ALRS", "TQBR", 'QUIK'],
            ["GAZP", "TQBR", 'QUIK'],
            # ["LKOH", "TQBR", 'QUIK'],
            # ["MGNT", "TQBR", 'QUIK'],
            # ["MOEX", "TQBR", 'QUIK'],
            # ["ROSN", "TQBR", 'QUIK'],
            ["SBER", "TQBR", 'QUIK'],
            # ["SBERP", "TQBR", 'QUIK'],
            # ["SNGS", "TQBR", 'QUIK'],
            # ["VTBR", "TQBR", 'QUIK'],

            ['B51 COMDTY', '', 'quantlab'],
            ['VE1 Index', '', 'quantlab'],
        ]

        request_format = ('Symbol', 'Board', 'Provider', 'TimeZone',
                'DateStart', 'DateEnd', 'BarScale', 'BarInterval',
                'IncludeNonTradeDays', 'IncludePartialBars',
                'IncludeEmptyBars', 'FilterTimeStart',
                'FilterTimeEnd', 'MaxBarsCount')


        FilterTimeStart = datetime.time(10, 0, 0).isoformat()
        FilterTimeEnd = datetime.time(18, 40, 0).isoformat()

        commonList = ['мск', today, today, 'Seconds', 1,
                      False, True, False, FilterTimeStart, FilterTimeEnd, None]
        for d_request in data_request:
            d_request += commonList

        result = {
            'request': [dict(zip(request_format, data)) for data in data_request]
        }

        return JsonResponse(result)
    elif request.method == 'POST':
        def handle_uploaded_file(f):
            with open('{0}/{1}.zip'.format(ALGOSITE_DATABOX, today), 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        if 'file' in request.FILES:
            handle_uploaded_file(request.FILES['file'])
            return HttpResponse('OK')
        else:
            return HttpResponse('FAIL')
