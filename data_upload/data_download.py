#coding=utf-8

import sys, os, django
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ['DJANGO_SETTINGS_MODULE'] = 'algosite.settings'
django.setup()

from algosite.settings import ALGOSITE_DATABOX, UNIVERSARY
import os, sys
from datetime import datetime as dt
from shutil import copyfile
import numpy as np
from subprocess import call

UNIVERSARY_FOLDER_CSV = os.path.join(ALGOSITE_DATABOX, UNIVERSARY['name'])
UNIVERSARY_FOLDER_NUMPY = os.path.join(UNIVERSARY_FOLDER_CSV, 'syncData')
DEST_FOLDER_NUMPY = os.path.join(ALGOSITE_DATABOX, 'doc_data', 'numpy')
DEST_FOLDER_CSV = os.path.join(ALGOSITE_DATABOX, 'doc_data', 'csv')

def yandexDiskUpload(fileSrc, password):
    import urllib2, base64
    opener = urllib2.build_opener(urllib2.HTTPHandler)

    uploadData = ""
    f = open(fileSrc, 'rb')
    length = 0
    while True:
       bytes = f.read(2048)
       uploadData += bytes;
       length += len(bytes)
       if not bytes: break
    f.close()

    fileName = fileSrc
    if '/' in fileSrc:
        fileName = fileSrc.split('/')[-1]
    request = urllib2.Request("https://webdav.yandex.ru/bcsquants/data/" + fileName, data=uploadData)
    base64string = base64.encodestring('%s:%s' % ("dmitriy.ivanovskiy@yandex.ru", password)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    request.add_header("Content-Length", "%d" % length)
    request.get_method = lambda: 'PUT'
    result = urllib2.urlopen(request)

def saveNumpy(startDate, lastDate, password):
    for tickSize in UNIVERSARY['tickSizes']:
        for instrument in UNIVERSARY['instruments']:
            ticker = instrument['symbol']

            folder = os.path.join(UNIVERSARY_FOLDER_NUMPY, tickSize, ticker)
            key = 'time'
            timeData = np.load(os.path.join(folder, key + '.npy'))
            startInd = -1
            lenData = len(timeData)
            for ind, t in enumerate(timeData):
                if t > lastDate:
                    resInd = ind
                    break
                if t >= startDate:
                    if startInd == -1:
                        startInd = ind

            for key in UNIVERSARY['keysData']:
                data = np.load(os.path.join(folder, key + '.npy'))
                assert(len(data) == lenData)
                directory = os.path.join(DEST_FOLDER_NUMPY, 'data', tickSize, ticker)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                np.save(os.path.join(directory, key + '.npy'), data[startInd:resInd])
            print(ticker, tickSize)

    zipName = 'dataNumpy.zip'
    call('cd {0}; zip {1} -r data'.format(DEST_FOLDER_NUMPY, zipName), shell=True)
    yandexDiskUpload(os.path.join(DEST_FOLDER_NUMPY, zipName), password)

def saveCsv(startDate, lastDate, password):
    for tickSize in UNIVERSARY['tickSizes']:
        for instrument in UNIVERSARY['instruments']:
            ticker = instrument['symbol']
            folderSrc = os.path.join(UNIVERSARY_FOLDER_CSV, tickSize, ticker)
            for csvFile in os.listdir(folderSrc):
                dateStr, _ = csvFile.split('.')
                dateKey = dt.strptime(dateStr, '%Y%m%d')

                folderDst = os.path.join(DEST_FOLDER_CSV, tickSize, ticker)
                if not os.path.exists(folderDst):
                    os.makedirs(folderDst)
                if startDate.date() <= dateKey.date() <= lastDate.date():
                    copyfile(os.path.join(folderSrc, csvFile), os.path.join(folderDst, csvFile))

    zipName = 'dataCsv.zip'
    call('cd {0}; zip {1} -r .'.format(DEST_FOLDER_CSV, zipName), shell=True)
    yandexDiskUpload(os.path.join(DEST_FOLDER_CSV, zipName), password)

def main():
    startDate = UNIVERSARY['dataConvertFunction'](UNIVERSARY['firstTestDatetime'])
    lastDate = UNIVERSARY['dataConvertFunction'](UNIVERSARY['lastTestDatetime'])
    saveNumpy(startDate, lastDate, sys.argv[1])
    saveCsv(startDate, lastDate, sys.argv[1])

if __name__ == '__main__':
    main()