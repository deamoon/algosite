# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os
from django.shortcuts import render, redirect
from bcs_client.models import AttemptBcs, Worker, AttemptResultBcs, StrategyBcs, Profile, Email
from django.contrib.auth.decorators import login_required
from django.core import serializers
import json, datetime
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from blog.models import ExampleStrategy
from django.core.validators import validate_email
from django.core.urlresolvers import reverse
from algosite.settings import BCS_QUANTS_CHALLENGE, PROJECT_ROOT, BASE_DIR
import bcs_client.signals

def merge_dict(a, b):
    """ Do update in-place and return result dictionary"""
    return a.update(b) or a

def append_list(a, b):
    """ Do update in-place and return result dictionary"""
    return a.append(b) or a

@login_required
def showAttemptResult(request):
    pass

@login_required
def getLanding(request):
    context = {}
    if request.user.is_authenticated():
        return render(request, 'bcs_client/landing.html', context)

@login_required
def getMain(request):
    context = {}
    if request.user.is_authenticated():
        return render(request, 'bcs_client/main.html', context)

@login_required
def getForm(request):
    attempts = AttemptBcs.objects.filter(user=request.user)
    context = {
        'languages': AttemptBcs.LANGUAGE_LIST,
        'code': ExampleStrategy.objects.get(id=1).code,
        'keyPage': 'getForm',
    }
    if attempts:
        context['code'] = attempts[0].code
        context['name'] = attempts[0].name
    return render(request, 'bcs_client/form.html', context)

@login_required
def sendAlgo(request):
    if request.method == 'POST':
        try:
            code = request.POST['code']
            language = request.POST['language']
            name = request.POST['name']
        except KeyError:
            return HttpResponse(status=400)

        strategy = StrategyBcs(name=name, user=request.user)
        strategy.save()

        attempt = AttemptBcs(name=name, code=code, language=language, status='S', user=request.user, strategy=strategy)
        attempt.save()

        strategy.bAttempt = attempt
        strategy.save()

        if not request.user.is_staff and AttemptBcs.objects.filter(status='S', user=request.user, mode='B', deleted=False).count() >= 3:
            attempt.status = 'E'
            attempt.error_message = 'У вас не менее 2-х стратегий, ожидающих завершения тестирования. Пожалуйста, дождитесь завершения тестирования и можете снова отправить стратегию'
            attempt.save()
        else:
            attempt.send()

        result = {
            'attemptId': attempt.id,
        }
        return redirect(reverse('bcs_client:getAttempt',kwargs={'attempt_id':attempt.id}))
    else:
        return HttpResponse(status=400)

def deleteAttempt(request, attempt_id):
    attempt = AttemptBcs.objects.get(id=attempt_id)
    if not request.user.is_staff and request.user.id != attempt.user.id:
        return HttpResponse(status=400)
    attempt.deleted = True
    attempt.save()
    return redirect('bcs_client:showAttempts')

@csrf_exempt
@login_required
def showAttempts(request):
    bAttemptKey = ['link', 'status', 'controlResult', 'testResult', 'sumProcent', 'numDeals', 'delete']
    oAttemptKey = ['link', 'onlineTicker', 'status', 'scale', 'sumProcent', 'numDeals', 'onlineDatetime']
    translateField = {
        'B': {
            'mode': 'Режим',
            'status': 'Статус',
            'language': 'Язык',
            'sumProcent': 'Средний P&L',
            'numDeals': 'Кол-во сделок',
            'controlResult': 'Контрольное качество',
            'link': 'Название',
            'addOnline': 'Перевести в онлайн',
            'testResult': 'Тестовое качество',
            'id': 'Id',
            'delete': 'Действие',
            'onlineTicker': 'Инструмент',
            "onlineDatetime": "Дата начала"
        }
    }
    oTranslateField = dict(translateField['B'])
    oTranslateField.update({
        'testResult': 'Онлайн качество',
        'sumProcent': 'P&L',
        'scale': 'Онлайн результат',
    })
    translateField['O'] = oTranslateField
    statusToHtml = {
        'O': '<span class="c-label">Тест пройден</span>',
        'E': '<span class="c-label -error">Ошибка</span>',
        'S': '<span class="c-label -disabled">Выполняется...</span>',
    }

    def attemptKeyView(key, value):
        if key in ("sumProcent", 'scale'):
            if value:
                return round(value * 100, 2)
        if key in ("controlResult", 'testResult'):
            if value:
                return round(value, 2)
        if key in ('onlineDatetime'):
            return value.date()
        return value

    def transferField(key, attempt):
        if key == 'link':
            if attempt.name:
                return '<a href="{0}"><span class="h-red">{1}</span></a>'.format(attempt.id, unicode(attempt.name))
            return '<a href="{0}"><span class="h-red">Attempt #{1}</span></a>'.format(attempt.id, attempt.id)
        if key == 'scale':
            jsonResult = AttemptResultBcs.objects.get(attempt=attempt, ticker=attempt.onlineTicker).jsonResult
            return attemptKeyView('scale', jsonResult['scale'])
        elif key == 'addOnline':
            if attempt.status == 'O':
                return '<a href="add_online/{0}">{1}</a>'.format(attempt.id, 'Добавить в online')
            return ''
        elif key == 'status':
            return statusToHtml.get(attempt.status, statusToHtml['E'])
        elif key == 'delete':
            return '<a href="{0}">Удалить</a>'.format(reverse('bcs_client:deleteAttempt',  kwargs={'attempt_id': attempt.id}))
        else:
            return attemptKeyView(key, getattr(attempt, key))

    if 'json' in request.GET.keys():
        result = {}
        mode = request.GET.get('mode', 'B')
        attempts = AttemptBcs.objects.filter(user=request.user, mode=mode, deleted=False).order_by('-id')
        if attempts:
            if mode == 'B':
                attemptKey = bAttemptKey
            else:
                attemptKey = oAttemptKey
            data = [[transferField(key, attempt) for key in attemptKey] for attempt in attempts]
        else:
            data = []

        result['data'] = data
        return JsonResponse(result)
    else:
        context = {
            'backtestTableKey': [translateField['B'][key] for key in bAttemptKey],
            'onlineTableKey': [translateField['O'][key] for key in oAttemptKey],
            'numBacktestAttempts': AttemptBcs.objects.filter(user=request.user, mode='B', deleted=False).count(),
            'numOnlineAttempts': AttemptBcs.objects.filter(user=request.user, mode='O', deleted=False).count(),
            'keyPage': 'showAttempts',
        }
        return render(request, 'bcs_client/attempt.html', context)

@csrf_exempt
@login_required
def getAttemptInfo(request, attempt_id):
    attempt = AttemptBcs.objects.get(id=attempt_id)
    if not request.user.is_staff and request.user.id != attempt.user.id:
        return HttpResponse(status=400)
    return JsonResponse({'status': attempt.status})

@csrf_exempt
@login_required
def getAttempt(request, attempt_id, context=None):
    commonRes = ['controlResult', 'testResult', 'sumProcent']
    tableKey = {
        'O': ["ticker", "scale", "sumProcent", "testResult",
            "maxDrawdown", #"sharpe",  # "minV",
            "numDeals", "sumTakeProfit", "sumHoldPeriod", "sumStopLoss"],
        'B': ["ticker", "controlResult", "testResult",
            "controlScale", "scale",
            "sumProcent",
            "maxDrawdown", # "sharpe",  # "minV",
            "numDeals", "sumTakeProfit", "sumHoldPeriod", "sumStopLoss"]
    }
    translateField = {
        'B': {
            'ticker': ('Инструмент', 'Название финансового инструмента, на котором получено данное качество'),
            'testResult': ('Тест. качество', 'Общая мера качества стратегии на тестовых данных </br>' +
                                              '(является функцией от соотношения прибыльных и убыточных сделок, </br>' +
                                              'суммарного P&L, средней прибыли и среднего убытка по сделкам и количества сделок)'),
            'controlResult': ('Контр. качество', 'Общая мера качества стратегии на контрольных данных </br>' +
                                                   '(является функцией от соотношения прибыльных и убыточных сделок, </br>' +
                                                   'суммарного P&L, средней прибыли и среднего убытка по сделкам и количества сделок)'),
            'sumProcent': ('P&L', 'Прибыль или убыток (если значение меньше 0) стратегии на всей тестовой выборке, в процентах'),
            'maxDrawdown': ('Макс. просадка', 'Максимальный убыток стратегии, достигнутый во время теста'),
            'std': ('Станд. откл.', 'Мера устойчивости величины прибыли во времени на протяжении тестирования'),
            'minV': ('Мин.', 'Минимум'),
            'numDeals': ('Кол-во сделок', 'Количество совершенных сделок, открытие и закрытие позиции учитываются как одна сделка'),
            'sumTakeProfit': ('Выход по TP', 'Прибыль или убыток по сделкам, закрывшимся по параметру takeProfit'),
            'sumHoldPeriod': ('Выход по HP', 'Прибыль или убыток по сделкам, закрывшимся по параметру holdingPeriod'),
            'sumStopLoss': ('Выход по SL', 'Прибыль или убыток по сделкам, закрывшимся по параметру stopLoss'),
            'tickFile': ('Заявки', 'Файл с заявками(вызовы функции order)'),
            'orderFile': ('Сделки', 'Файл со сделками(заявки, которые были учтены бэктестом)'),
            'sharpe': ('Тест. резул.', 'Величина, учитывающая отношение средней доходности к риску на тестовой'),
            'scale': ('Тест. резул.', 'Величина, корректирующая результирующую доходность с учётом просадок на тестовой выборке. Главный критерий в онлайне'),
            'controlScale': ('Контр. резул.', 'Величина, корректирующая результирующую доходность с учётом просадок на контрольной выборке. Главный критерий в онлайне'),
        }
    }
    convertInstrument = {
        'USD000UTSTOM': 'USD',
        'MICEXINDEXCF': 'MICEX',
    }

    oTranslateField = dict(translateField['B'])
    oTranslateField.update({
        'testResult': ('Онлайн качество', 'Онлайн качество'),
        'sharpe': ('Результат', 'Величина, учитывающая отношение средней доходности к риску'),
        'scale': ('Результат', 'Величина, корректирующая результирующую доходность с учётом просадок'),
    })
    translateField['O'] = oTranslateField
    newKey = ['tickFile', 'orderFile']

    def newInfo(attemptResult):
        email = '<a href="{0}">{1}</a>'
        attempt = attemptResult.attempt
        prefix = '/{0}/attempt/{1}/order/{2}'.format(attempt.worker.name, attempt.workerAttemptId, attemptResult.ticker)
        return [email.format(prefix + '_tick.csv', 'Скачать'), email.format(prefix + '_order.csv', 'Скачать')]

    def myKeyView(key, value):
        value = value or 0
        if key in ("sumProcent", "scale", "controlScale",
                   "maxDrawdown", "std", "minV",
                   "sumTakeProfit", "sumHoldPeriod", "sumStopLoss", "sharpe",):
            if value:
                return round(float(value) * 100, 2)
        if key in ("testResult", "controlResult"):
            if value:
                return round(float(value), 2)
        if key in ("ticker"):
            if value in convertInstrument:
                value = convertInstrument[value]
            return '<a href="#" id="link_{0}" onclick="addChart(\'{0}\'); return false">{0}</a>'.format(value)
        return value

    attempt = AttemptBcs.objects.get(id=attempt_id)
    if not request.user.is_staff and request.user.id != attempt.user.id:
        return HttpResponse(status=400)

    attempt.pollResult()
    result = {}
    mode = attempt.mode or 'B'
    if mode == 'O' and not request.user.is_staff:
        data = AttemptResultBcs.objects.filter(attempt=attempt, ticker=attempt.onlineTicker).order_by('id')
    else:
        data = AttemptResultBcs.objects.filter(attempt=attempt).order_by('id')
    numberOnlineAttempt = BCS_QUANTS_CHALLENGE['numberOnlineAttempt']
    if request.user.is_staff:
        numberOnlineAttempt = 1000

    if 'json' in request.GET.keys():
        if attempt.status == 'E':
            result['error_message'] = attempt.error_message
        elif attempt.status in ('O', 'S'):
            result['data'] = [[myKeyView(key, attemptResult.jsonResult.get(key, 0)) for key in tableKey[mode]] + newInfo(attemptResult) for attemptResult in data]

        return JsonResponse(result)
    else:
        onlineCount = AttemptBcs.objects.filter(user=request.user, mode='O', deleted=False).count()
        onlineResult = 0
        if attempt.mode == 'O':
            try:
                jsonResult = AttemptResultBcs.objects.get(attempt=attempt, ticker=attempt.onlineTicker).jsonResult
                onlineResult = myKeyView('scale', jsonResult['scale'])
            except Exception as e:
                onlineResult = 0
        if not context:
            context = {}
        context.update({
            'attempt': attempt,
            'tableKey': [translateField[mode][key] for key in tableKey[mode]] + [translateField[mode][key] for key in newKey],
            'keyPage': 'getAttempt',
            'commonRes': {key:myKeyView(key, getattr(attempt, key, 0)) for key in commonRes},
            'tickers': [attemptResult.ticker for attemptResult in data],
            'freeOnlineCount': numberOnlineAttempt - onlineCount - 1,
            'onlineResult': onlineResult,
        })
        return render(request, 'bcs_client/result.html', context)

@csrf_exempt
def getAttemptWithoutLogin(request):
    attempt = AttemptBcs.objects.get(id=int(request.POST['attemptId']))
    print('getAttemptWithoutLogin {0}'.format(attempt.id))
    attempt.worker.workerFreeNum += 1
    attempt.worker.save()
    attempt.pollResult()
    profile = Profile.objects.get(user=attempt.user)
    profile.updateRating()
    return JsonResponse({'status': 'O'})

# deprecated?
@csrf_exempt
def pollAttempt(request):
    result = {}
    if 'attemptId' in request.GET.keys() and request.GET['attemptId']:
        attempt = AttemptBcs.objects.get(id=request.GET['attemptId'])
        attempt.pollResult()

        data = serializers.serialize("json", AttemptResultBcs.objects.filter(attempt=attempt))
        # print(data)
        result['data'] = data

        result['status'] = attempt.status
        result['error_message'] = attempt.error_message
        result['data'] = [merge_dict(res['fields'], {'id':res['pk']}) for res in json.loads(data) if 'fields' in res]

    return JsonResponse(result)

@login_required
def addOnline(request):
    attempt_id = int(request.POST['attempt_id'])
    profile = Profile.objects.get(user=request.user)
    attempt = AttemptBcs.objects.get(id=attempt_id)
    ticker = request.POST.get('ticker', None)
    if not request.user.is_staff and request.user.id != attempt.user.id:
        return HttpResponse(status=400)
    if not ticker:
        return getAttempt(request, attempt_id, context={'errorSendTicker': 'Выберите инструмент'})
    if attempt.testResult < 1 or attempt.controlResult < 1:
        return getAttempt(request, attempt_id, context={'errorSendTicker': 'Ваше тестовое и контрольное качество должны быть не меньше 1'})

    if attempt.status == 'O':
        onlineCount = AttemptBcs.objects.filter(user=request.user, mode='O', deleted=False).count()
        if BCS_QUANTS_CHALLENGE['numberOnlineAttempt'] - onlineCount > 0:
            newAttempt = AttemptBcs(name=attempt.name, code=attempt.code, language=attempt.language,
                                    status='S', user=request.user, strategy=attempt.strategy)
            newAttempt.mode = 'O'
            newAttempt.onlineTicker = ticker
            newAttempt.onlineDatetime = datetime.datetime.now()
            newAttempt.testResult = newAttempt.controlResult = newAttempt.numDeals = newAttempt.sumProcent = 0
            newAttempt.backtestAttempt = attempt
            newAttempt.save()

        profile.numberOnlineAttempts = AttemptBcs.objects.filter(user=request.user, mode='O', deleted=False).count()
        profile.save()
    return redirect('bcs_client:showAttempts')

@login_required
def updateProfile(request):
    user = request.user
    profile = Profile.objects.get(user=request.user)
    # profile.first_name = request.POST['first_name'] or profile.first_name
    # profile.last_name = request.POST['last_name'] or profile.last_name
    profile.isSubscribe = ('isSubscribe' in request.POST)
    profile.save()

    if 'cv' in request.FILES:
        cv = request.FILES['cv']
        if '.pdf' in cv.name or '.doc' in cv.name:
            if cv.size <= 5 * 1024 * 1024:
                if profile.cv:
                    if os.path.isfile(profile.cv.path):
                        os.remove(profile.cv.path)
                profile.cv = cv
                ext = cv.name.split('.')[-1]
                profile.cv.name = 'cv_{profileId}_{email}.{ext}'.format(email=user.email, profileId=profile.id, ext=ext)
                profile.save()
            else:
                return getProfile(request, extraFields={'error_profile_message': 'Размер файла должен быть меньше 5 MB'})
        else:
            return getProfile(request, extraFields={'error_profile_message': 'Файл должен быть формата .pdf или .doc(x)'})

    return redirect('bcs_client:profile')

@login_required
def getProfile(request, extraFields={}):
    context = {
        'keyPage': 'profile',
        'user': request.user,
        'profile': Profile.objects.get(user=request.user),
    }
    context.update(extraFields)
    return render(request, 'bcs_client/profile.html', context)

@login_required
def changePassword(request):
    if request.method == 'POST':
        context = {}
        user = request.user
        if request.POST['password1'] == request.POST['password2']:
            password = request.POST['password1']
            if password:
                user.set_password(password)
                user.save()
                return getProfile(request, extraFields={'success_message': 'Пароль изменён'})
            else:
                return getProfile(request, extraFields={'error_message': 'Пароль не может быть пустым'})
        else:
            return getProfile(request, extraFields={'error_message': 'Пароли не совпадают'})

@login_required
def profile(request):
    if request.method == 'POST':
        if request.POST['form_type'] == 'change_password':
            return changePassword(request)
        elif request.POST['form_type'] == 'change_profile':
            return updateProfile(request)
    else:
        return getProfile(request)

# @login_required
def rating(request):
    tableKey = ['place', 'name', 'numAttempts', 'onlineResult', 'backtestResult']
    translateField = {
        'place': 'Место',
        'name': 'Участник',
        'numAttempts': 'Стратегий онлайн',
        'onlineResult': 'Рейтинг онлайн',
        'backtestResult': 'Рейтинг бэктест'
    }

    userResult = []
    userProfile = None
    result = []
    for ind, profile in enumerate(Profile.objects.filter(showInRating=True).order_by('-rating')):
        line = [
            ind + 1,
            profile.first_name,
            AttemptBcs.objects.filter(user=profile.user, mode='O', deleted=False).count(),
            round(profile.rating, 2),
            round(profile.backtestRating, 2),
        ]
        if profile.user == request.user:
            userResult = line
            userProfile = profile
        result.append(line)

    result.sort(key=lambda x: (-bool(x[2]), -x[3], -bool(x[4]), -x[4]))

    for ind, person in enumerate(result):
        person[0] = ind + 1

    if 'json' in request.GET.keys():
        return JsonResponse({
            'data': result,
        })
    else:
        context = {
            'keyPage': 'rating',
            'tableKey': [translateField[key] for key in tableKey],
            'userResult': userResult,
            'userProfile': userProfile,
        }
        return render(request, 'bcs_client/rating.html', context)

@csrf_exempt
def add_email(request):
    try:
        ErrorResponse = JsonResponse({'error': 'Ошибка отправки'})
        if request.method == 'POST':
            email = request.POST['email']
            validate_email(email)
            count = Email.objects.all().count()
            if count < 10000: # TODO
                if not Email.objects.filter(email=email).exists():
                    Email(email=email).save()
                return JsonResponse({'message': 'Запрос отправлен, спасибо'})
        return ErrorResponse
    except Exception as e:
        return ErrorResponse

def getDocMain(request):
    return redirect(reverse('bcs_client:getDoc',kwargs={'docName':'main'}))

def getDoc(request, docName):
    docFolder = os.path.join(BASE_DIR, 'templates', 'doc')
    with open(os.path.join(docFolder, 'book.json')) as file:
        config = json.load(file)
    menuNames, menuUrls, menuFiles = zip(*config['bcsquantsMenu'])
    for docFile, docUrl in zip(menuFiles, menuUrls):
        if docUrl == docName:
            with open(os.path.join(docFolder, docFile + '.html')) as file:
                content = file.read()
            context = {
                'keyPage': 'doc',
                'config': config,
                'content': content.decode('utf-8'),
                'menu': config['bcsquantsMenu'],
                'docName': docName,
            }
            return render(request, 'bcs_client/doc.html', context)
    return HttpResponse(status=400)

import subprocess
@login_required
def compileDoc(request):
    if request.user.is_staff:
        folder = os.path.join(os.path.dirname(BASE_DIR), 'bcsquants_doc')
        script = os.path.join(folder, 'compile.sh')
        result = subprocess.check_output('./compile.sh', cwd=folder)
        return JsonResponse({'result': result})
    else:
        return JsonResponse({'result': 'Доступ запрещён'})

# @login_required
def index_main(request):
    # if request.user.is_authenticated():
    #     context = {
    #         'footer_not_fix': False,
    #     }
    #     return render(request, 'staticpage/index_login.html', context)
    # else:
    #     context = {
    #         'footer_not_fix': True,
    #     }
    # if request.user.is_authenticated():
    #     return redirect('bcs_client:getMain')
    return render(request, 'staticpage/bcs_landing.html')