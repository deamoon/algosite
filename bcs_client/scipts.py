#coding=utf-8

import sys, os, django
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ['DJANGO_SETTINGS_MODULE'] = 'algosite.settings'
django.setup()



if __name__ == '__main__':
    updateRatingAll()