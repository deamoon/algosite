# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.dispatch import receiver
from django.db.models.signals import post_delete, post_save
from bcs_client.models import AttemptBcs, Profile

# Don't forget to import this file somewhere(e.x in views.py)
@receiver(post_save, sender=AttemptBcs)
@receiver(post_delete, sender=AttemptBcs)
def numberOnlineAttemptsUpdate(sender, **kwargs):
    instance = kwargs['instance']
    try:
        profile = Profile.objects.get(user=instance.user)
        profile.numberOnlineAttempts = AttemptBcs.objects.filter(user=instance.user, mode='O', deleted=False).count()
        profile.save()
    except Exception as e:
        pass