# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import requests
from django.contrib.postgres.fields import JSONField
import random, datetime
from django.utils.encoding import python_2_unicode_compatible
import traceback
import random

BCS_TEST_URL = 'history/bcs_eh1hfwWDcsa8fe'
BCS_POLL_URL = 'history/poll_nwijhef2f342f'

class Worker(models.Model):
    STATUS_LIST = (
        ('O', 'OK'),
        ('E', 'Error'),
        ('W', 'Waiting') # stop for support working
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST)

    name = models.CharField(max_length=50)
    url = models.CharField(max_length=50)
    urlStatic = models.CharField(max_length=50)
    procNum = models.PositiveSmallIntegerField(null=True, blank=True)
    memoryNum = models.PositiveSmallIntegerField(null=True, blank=True)
    hddNum = models.PositiveSmallIntegerField(null=True, blank=True)

    workerFreeNum = models.SmallIntegerField(null=True, blank=True)
    workerAllNum = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return "{0} {1} {2}/{3}".format(self.name, self.url, self.workerFreeNum, self.workerAllNum)

    def save(self, *args, **kwargs):
        url = self.url
        if 'http' not in url:
            url = 'http://' + url
        if url[-1] == '/':
            url = url[:-1]
        self.url = url

        super(Worker, self).save(*args, **kwargs)

    @staticmethod
    def getBestWorker():
        workers = list(Worker.objects.filter(status='O'))
        random.shuffle(workers)
        # workers.sort(key=lambda x:x.workerFreeNum, reverse=True) # TODO, not work properly

        for worker in workers:
            if worker.checkWorker():
                return worker

        # TODO, send email to admin

        random.shuffle(workers)
        for worker in workers:
            if worker.checkWorker():
                return worker

        # TODO, send email to admin
        return None

    def sendAttempt(self, attempt):
        try:
            if self.checkWorker():
                url = '{0}/{1}'.format(self.url, BCS_TEST_URL)
                attemptRes = requests.post(url, data={'code':attempt.code,
                                                      'language':attempt.language,
                                                      'attemptId':attempt.id,
                                                      'onlineDatetime':attempt.onlineDatetime})
                print('attemptRes.text = ', attemptRes.text, url)
                attempt.workerAttemptId = attemptRes.json()['attemptId']
                attempt.worker = self
                attempt.status = 'S'

                if self.workerFreeNum > 0:
                    self.workerFreeNum -= 1
                    self.save()
                else:
                    # TODO, send to admin
                    pass

        except Exception as e:
            attempt.status = 'E'
            attempt.error_message += traceback.format_exc()
            # raise e
        finally:
            attempt.save()

    def pollAttempt(self, attempt):
        urlPoll = '{0}/{1}'.format(self.url, BCS_POLL_URL)

        if self.checkWorker():
            print('requests.post', urlPoll)
            attemptRes = requests.get(urlPoll, params={'attemptId':attempt.workerAttemptId})
            result = attemptRes.json()
            return result

        return None

    def checkWorker(self):
        try:
            urlTest = '{0}/{1}'.format(self.url, BCS_TEST_URL)
            print('requests.get(url), ' + urlTest)
            r = requests.get(urlTest).json()
            return r['isAllOk']
        except Exception as e:
            return False

@python_2_unicode_compatible
class StrategyBcs(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    bAttempt = models.OneToOneField('bcs_client.AttemptBcs', blank=True, null=True, related_name='bAttempts_set')
    oAttempt = models.OneToOneField('bcs_client.AttemptBcs', blank=True, null=True, related_name='oAttempts_set')
    oTicker = models.CharField(max_length=20, blank=True, null=True)
    MODE_LIST = (
        ('O', 'online'),
        ('B', 'backtest'),
    )
    mode = models.CharField(max_length=20, choices=MODE_LIST, default='B')
    onlineDatetime = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(User, related_name='strategys')

    class Meta:
        ordering = ['-pk']

    def save(self, *args, **kwargs):
        name = getattr(self, 'name', '')
        if not name:
            self.name = 'Strategy #{0}'.format(StrategyBcs.objects.filter(user=self.user).count() + 1)
        super(StrategyBcs, self).save(*args, **kwargs)

    def __str__(self):
        return self.name + ' ' + self.user.username

@python_2_unicode_compatible
class AttemptBcs(models.Model):
    STATUS_LIST = (
        ('S', 'Sending'),
        ('O', 'OK'),
        ('E', 'Error')
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST)

    deleted = models.BooleanField(default=False)

    code = models.TextField()
    error_message = models.TextField(default="", blank=True, null=True)

    LANGUAGE_LIST = (
        # ('python2', 'Python 2.7.6'),
        ('python3', 'Python 3.4.3'),
    )
    language = models.CharField(max_length=20, choices=LANGUAGE_LIST)
    name = models.CharField(max_length=200, blank=True, null=True)

    workerAttemptId = models.PositiveIntegerField(blank=True, null=True)
    worker = models.ForeignKey(Worker, blank=True, null=True)
    user = models.ForeignKey(User)
    strategy = models.ForeignKey(StrategyBcs, related_name='attempts')

    testResult = models.FloatField(blank=True, null=True)
    controlResult = models.FloatField(blank=True, null=True)
    numDeals = models.PositiveIntegerField(blank=True, null=True)
    sumProcent = models.FloatField(blank=True, null=True)

    backtestAttempt = models.ForeignKey('bcs_client.AttemptBcs', blank=True, null=True)
    onlineTicker = models.CharField(max_length=30)
    onlineDatetime = models.DateTimeField(blank=True, null=True)
    updateDatetime = models.DateTimeField(auto_now=True)

    MODE_LIST = (
        ('O', 'online'),
        ('B', 'backtest'),
    )
    mode = models.CharField(max_length=20, choices=MODE_LIST, default='B')

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return "Attempt #" + str(self.id) + " status " + self.status + ' mode ' + self.mode + \
               ' ' + self.user.profile.first_name + ', ' + str(self.updateDatetime)

    def save(self, *args, **kwargs):
        name = getattr(self, 'name', '')
        if not name:
            self.name = 'Attempt #{0}'.format(AttemptBcs.objects.filter(user=self.user).count() + 1)
        super(AttemptBcs, self).save(*args, **kwargs)

    def pollResult(self):
        if self.status == 'S':
            try:
                if not self.worker:
                    "TODO"
                    return

                jsonResult = self.worker.pollAttempt(self)
                if jsonResult:
                    if jsonResult['status'] == 'E':
                        self.status = 'E'
                        self.error_message = jsonResult['error_message']
                        return

                    self.numDeals = self.controlResult = self.sumProcent = 0
                    for res in jsonResult['data']:
                        if self.mode == 'O':
                            if res['ticker'] == self.onlineTicker:
                                self.numDeals = res['numDeals']
                                self.sumProcent = res['sumProcent']
                                self.testResult = res['testResult']
                                self.controlResult = 0
                        else:
                            self.numDeals += res['numDeals']
                            self.sumProcent += res['sumProcent']
                        if AttemptResultBcs.objects.filter(ticker=res['ticker'], attempt=self).exists():
                            resultBcs = AttemptResultBcs.objects.get(ticker=res['ticker'], attempt=self)
                            resultBcs.jsonResult = res
                        else:
                            # print res
                            resultBcs = AttemptResultBcs(jsonResult=res, ticker=res['ticker'], attempt=self)
                        resultBcs.save()

                    if jsonResult['status'] == 'O':
                        if self.mode == 'B':
                            testResults = []
                            controlResults = []
                            for res in jsonResult['data']:
                                testResults.append(res['testResult'])
                                controlResults.append(res['controlResult'])
                            testResults.sort(reverse=True)
                            self.testResult = (sum(testResults) / float(len(testResults)) + sum(testResults[:3]) / 3.) / 2.
                            controlResults.sort(reverse=True)
                            self.controlResult = (sum(controlResults) / float(len(controlResults)) + sum(controlResults[:3]) / 3.) / 2.
                            numFinishedTickers = len(jsonResult['data'])
                            self.sumProcent = self.sumProcent / float(numFinishedTickers)

                        self.status = 'O'
            except Exception as e:
                self.status = 'E'
                self.error_message += traceback.format_exc()
            finally:
                self.save()

    def send(self):
        self.status = 'S'
        self.save()
        worker = Worker.getBestWorker()
        if worker:
            worker.sendAttempt(self)
        else:
            self.status = 'E'
            self.error_message = 'Кажется у нас нет доступных серверов для тестирования, пожалуйста повторите попытку позже'
            self.save()

    def sendOnline(self):
        if self.status == 'O':
            self.mode = 'O'
            self.onlineDatetime = datetime.datetime.now()
            self.save()
        else:
            print('Attempt with error status')
            # TODO logs

class AttemptResultBcs(models.Model):
    jsonResult = JSONField(blank=True, null=True)

    ticker = models.CharField(max_length=30)
    # TYPE_RESULT = (
    #     ('O', 'Online'),
    #     ('C', 'Control'),
    #     ('T', 'Test') # stop for working
    # )
    # typeResult = models.CharField(max_length=20, choices=TYPE_RESULT)

    attempt = models.ForeignKey(AttemptBcs, blank=True, null=True, related_name='attemptResults')

    def __str__(self):
        return "#{0} {1}".format(self.attempt.id, self.ticker)

    class Meta:
        ordering = ['pk']

@python_2_unicode_compatible
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')

    rating = models.FloatField(default=0.0)
    backtestRating = models.FloatField(default=0.0)
    numberOnlineAttempts = models.IntegerField(default=0, blank=True)

    first_name = models.CharField(max_length=100, default='', blank=True)
    last_name = models.CharField(max_length=100, default='', blank=True)
    middle_name = models.CharField(max_length=100, default='', blank=True)
    birth_day = models.CharField(max_length=100, default='', blank=True)
    sex = models.CharField(max_length=100, default='', blank=True)
    city = models.CharField(max_length=100, default='', blank=True)
    school = models.CharField(max_length=100, default='', blank=True)
    department = models.CharField(max_length=100, default='', blank=True)
    speciality = models.CharField(max_length=100, default='', blank=True)
    degree = models.CharField(max_length=100, default='', blank=True)
    graduate_year = models.CharField(max_length=100, default='', blank=True)
    cv = models.FileField(upload_to='cv/', blank=True, null=True)
    isSubscribe = models.BooleanField(default=True)

    showInRating = models.BooleanField(default=True)

    def __str__(self):
        return "#{0} {1}".format(self.user.username, self.rating)

    class Meta:
        ordering = ['-rating']

    def updateRating(self):
        if self.numberOnlineAttempts > 0:
            self.rating = 0
            for attempt in AttemptBcs.objects.filter(user=self.user, mode='O', deleted=False):
                onlineScale = 0
                try:
                    onlineRes = AttemptResultBcs.objects.get(ticker=attempt.onlineTicker, attempt=attempt).jsonResult
                    if 'scale' in onlineRes:
                        onlineScale = onlineRes['scale']
                except AttemptResultBcs.DoesNotExist:
                    pass
                self.rating += onlineScale
            self.rating *= 10.

        self.backtestRating = 0
        scaleList = []
        for attempt in AttemptBcs.objects.filter(user=self.user, mode='B', deleted=False):
            scale, controlScale = 0, 0
            for result in AttemptResultBcs.objects.filter(attempt=attempt):
                jsonResult = result.jsonResult
                if 'scale' in jsonResult:
                    scale += jsonResult['scale']
                if 'controlScale' in jsonResult:
                    controlScale += jsonResult['controlScale']
            # scaleList.append(scale + controlScale)
            scaleList.append(controlScale)
        scaleList.sort(reverse=True)
        sharpeListShort = scaleList[:min(10, len(scaleList))]
        self.backtestRating = sum(sharpeListShort)

        self.save()

@python_2_unicode_compatible
class Email(models.Model):
    email = models.EmailField(unique=True)

    def __str__(self):
        return "{0}".format(self.email)