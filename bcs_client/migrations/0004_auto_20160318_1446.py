# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-18 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bcs_client', '0003_auto_20160317_0256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='worker',
            name='workerFreeNum',
            field=models.SmallIntegerField(blank=True, null=True),
        ),
    ]
