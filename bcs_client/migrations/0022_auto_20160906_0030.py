# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-09-05 21:30
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bcs_client', '0021_auto_20160905_0235'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attemptbcs',
            options={'ordering': ['-pk']},
        ),
        migrations.AlterModelOptions(
            name='attemptresultbcs',
            options={'ordering': ['pk']},
        ),
        migrations.AlterModelOptions(
            name='strategybcs',
            options={'ordering': ['-pk']},
        ),
        migrations.AlterModelOptions(
            name='userbcs',
            options={'ordering': ['-rating']},
        ),
        migrations.AlterField(
            model_name='attemptbcs',
            name='strategy',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attempts', to='bcs_client.StrategyBcs'),
        ),
        migrations.AlterField(
            model_name='attemptresultbcs',
            name='attempt',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='attemptResults', to='bcs_client.AttemptBcs'),
        ),
        migrations.AlterField(
            model_name='userbcs',
            name='rating',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='userbcs',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='userBcs', to=settings.AUTH_USER_MODEL),
        ),
    ]
