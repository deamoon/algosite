# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-24 12:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bcs_client', '0005_worker_urlstatic'),
    ]

    operations = [
        migrations.AddField(
            model_name='attemptbcs',
            name='name',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
    ]
