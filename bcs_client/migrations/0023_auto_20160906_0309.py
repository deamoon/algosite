# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-09-06 03:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcs_client', '0022_auto_20160906_0030'),
    ]

    operations = [
        migrations.RenameField(
            model_name='strategybcs',
            old_name='oInstrument',
            new_name='oTicker',
        ),
    ]
