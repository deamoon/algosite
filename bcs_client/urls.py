from django.conf.urls import patterns, url, include
import views
import os
import json
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^profile/$', views.profile, name='profile'),

    url(r'^rating/$', views.rating, name='rating'),
    url(r'^attempt/$', views.showAttempts, name='showAttempts'),
    url(r'^send_algo/$', views.sendAlgo, name='sendAlgo'),
    url(r'^send/$', views.getForm, name='getForm'),
    url(r'^main/$', views.getMain, name='getMain'),
    url(r'^landing/$', views.getLanding, name='getLanding'),

    url(r'^doc/$', views.getDocMain, name='getDocMain'),
    url(r'^doc/(?P<docName>[\w-]+)/$', views.getDoc, name='getDoc'),
    url(r'^doc/z7Ganc864/compile/$', views.compileDoc, name='compileDoc'),

    url(r'^attempt/(?P<attempt_id>[0-9]+)/info/$', views.getAttemptInfo, name='getAttemptInfo'),
    url(r'^attempt/(?P<attempt_id>[0-9]+)/$', views.getAttempt, name='getAttempt'),
    url(r'^attempt/(?P<attempt_id>[0-9]+)/chart/', include('tradingview.urls', namespace='tradingview')),

    url(r'^attempt/bcs_nfe76Rtfs2vk/$', views.getAttemptWithoutLogin, name='getAttemptWithoutLogin'),
    url(r'^attempt/delete/(?P<attempt_id>[0-9]+)/$', views.deleteAttempt, name='deleteAttempt'),
    url(r'^attempt/add_online/$', views.addOnline, name='addOnline'),

    url(r'^add_email/$', views.add_email, name='add_email'),

    url(r'^$', views.index_main, name='index'),

    url(r'^team/$', TemplateView.as_view(template_name='bcs_client/team.html'), name='team'),
    url(r'^agreement/$', TemplateView.as_view(template_name='bcs_client/agreement.html'), name='agreement'),
]