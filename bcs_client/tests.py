# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth.models import AnonymousUser, User
from bcs_client.models import AttemptBcs, StrategyBcs, Worker
from time import sleep

class AnimalTestCase(TestCase):
    codeSimple = '''
def init(self):
    self.alpha = 0.02 # 1 процент
    self.holdPeriod = 300 # 300 тиков, не менее 20
    self._tickSize = 'm5' # 's1', 's5', 'm1', 'm5'; default = 'm5'
    self._window = 100 # use None or just delete line for infinity window

def tick(self, data):
    currentPrice = data['close'][-1] # последняя цена
    mean = data['close'].mean()

    if currentPrice >= (1 + self.alpha) * mean:
        order('buy', takeProfit=self.alpha, stopLoss=self.alpha/2, holdPeriod=self.holdPeriod)

    if currentPrice <= (1 - self.alpha) * mean:
        order('sell', takeProfit=self.alpha, stopLoss=self.alpha/2, holdPeriod=self.holdPeriod)
    '''

    def setUp(self):
        self.user = User.objects.create_user(username='deamoon', email='d@m.ru', password='top_secret')
        self.strategy = StrategyBcs(name='test_strategy', user=self.user)
        self.strategy.save()
        Worker(name='backtest1', url='http://40.118.71.134:8000', urlStatic='http://40.118.71.134', status='O').save()
        self.attempt = AttemptBcs(code=self.codeSimple,
                             language='python3',
                             status='S',
                             user=self.user,
                             strategy=self.strategy)
        self.attempt.send()


    def testSendSimpleAttempt(self):
        sleep(10)
        self.assertEqual(self.attempt.status, 'O')
