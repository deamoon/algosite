from django import forms
from bcs_client.models import Profile

class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=100, required=True)
    # rules = forms.BooleanField(required=True)
    # last_name = forms.CharField(max_length=30, label='Achternaam')

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.save()
        Profile(user=user, first_name=user.first_name).save()