#!/usr/bin/python
#coding=utf-8

import quant_pexpect as qexpect
import re
from attempt.models import listBuySell, AttemptResult
from celery import current_task
from sandbox.exceptions import CheckError
import numpy as np

class BactTest(object):
    def __init__(self, money, stock, comission):
        self.money = money
        self.stock = stock
        self.comission = comission

    def run(self, price_data, events):
        self.portfolio = np.zeros(len(price_data))
        money = self.money
        stock = self.stock
        ind_event = 0 # Индекс ближайшего события

        for num, price in enumerate(price_data):
            price = float(price)
            self.portfolio[num] = money + stock*price
            if ind_event < len(events) and events[ind_event].num == num:
                event = events[ind_event]
                order_price = event.quantity * event.price
                money -= order_price
                money -= abs(order_price) * self.comission
                stock += event.quantity

                errors = []
                if money + stock*price < 0:
                    errors.append("Not enough money on the balance")
                # if stock < 0:
                #     errors.append("Not enough stocks on the balance")

                ind_event += 1

    def getResultStat(self):
        returns = np.diff(self.portfolio) / self.portfolio[:-1]
        average_return = np.mean(returns)
        dispersion = np.std(returns)
        sharp = 0.
        if dispersion > 0.00001:
            sharp = np.sqrt(len(returns)) * (average_return / dispersion)

        end = np.argmax(np.maximum.accumulate(self.portfolio) - self.portfolio) # end of the period
        if end == 0:
            start = 0
        else:
            start = np.argmax(self.portfolio[:end]) # start of period
        max_drawdown = self.portfolio[start] - self.portfolio[end]

        attemptResult = AttemptResult(final_portfolio = self.portfolio[-1],
                                      dispersion = dispersion,
                                      average_return = average_return,
                                      sharp = sharp,
                                      portfolio = list(self.portfolio),
                                      max_drawdown = max_drawdown,)
        return attemptResult

class Event(object):
    pass

class BuySellEvent(Event):
    def __init__(self, price, quantity, num):
        self.price = price
        self.quantity = quantity
        self.num = num

class StrategyRunner(object):
    pass

class InteracriveStrategyRunner(StrategyRunner):
    def __init__(self, cmd, time_step_limit):
        self.cmd = cmd
        self.time_step_limit = time_step_limit

    def getEventsFromInteractiveTesting(self, price_data):
        child = qexpect.spawn(self.cmd, timeout=self.time_step_limit+2) # добавляем 2 секунды, время на запуск
        try:
            cpl = child.compile_pattern_list([qexpect.EOF,  qexpect.TIMEOUT, re.compile("buy .+"),
                                              re.compile("sell .+"), "skip"])

            child.sendline("{0}".format(len(price_data)))

            events = []
            for num, line in enumerate(price_data):
                line = line.strip()
                child.sendline(line)
                price = float(line) # сейчас в файле данных просто столбец цен

                try:
                    i = child.expect_list(cpl, timeout=self.time_step_limit)
                except Exception:
                    raise CheckError("Timeout of \'order\' or \'skip\'")

                if i == 0:
                    raise CheckError('EOF')
                elif i == 1:
                    raise CheckError('TIMEOUT')
                elif i == 2:
                    try:
                        quantity = float(child.after.split()[1])
                        events.append(BuySellEvent(price, quantity, num))
                    except Exception:
                        raise CheckError("Error in format, %s" % child.after)
                elif i == 3:
                    try:
                        quantity = float(child.after.split()[1])
                        events.append(BuySellEvent(price, -quantity, num))
                    except Exception:
                        raise CheckError("Error in format, %s" % child.after)
                elif i == 4:
                    # get skip event
                    pass
                else:
                    raise CheckError('Error checking system(qexpect)')

                current_task.update_state(state='PROGRESS',
                                          meta={'process_percent': int(100 * float(num) / len(price_data))})
            return events
        finally:
            child.terminate(force=True)
            child.close()

def run_test(cmd, attempt):
    money = attempt.contest.start_balance_money
    stock = attempt.contest.start_balance_stock
    comission = attempt.contest.comission / 100. # переводим проценты в число
    time_step_limit = attempt.contest.time_step_limit

    runner = InteracriveStrategyRunner(cmd, time_step_limit)
    bactTest = BactTest(money, stock, comission)

    price_data = attempt.market_data.getOpenData()

    events = runner.getEventsFromInteractiveTesting(price_data)
    bactTest.run(price_data, events)
    result = bactTest.getResultStat()
    result.save()
    attempt.result_open = result

def main():
    money = 1000.
    stock = 0.
    comission = 0.2 / 100. # переводим проценты в число
    time_step_limit = 10

    runner = InteracriveStrategyRunner('/home/deamoon/Documents/algotrading/static_content/quantbox/robot14.py', time_step_limit)
    bactTest = BactTest(money, stock, comission)

    with open('/home/deamoon/Documents/algotrading/static_content/media/openData/data_real_10k.csv') as file:
        price_data = file.readlines()

    events = runner.getEventsFromInteractiveTesting(price_data)

if __name__ == '__main__':
    main()