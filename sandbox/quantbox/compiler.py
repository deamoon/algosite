import subprocess

def compile(cmd):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    proc.wait()
    (out, err) = proc.communicate()
    return (out, err)

def main():
    out, err = compile("g++ 1.cpp")
    print err

if __name__ == '__main__':
    main()
