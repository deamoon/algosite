#!/usr/bin/python
#coding=utf-8

import os
from languages import *
from quantbox import runner, compiler
from algosite.settings import ALGOSITE_SANDBOX
from sandbox.exceptions import CheckError

SANDBOX = ALGOSITE_SANDBOX

def writeToFolder(dir, name, code):
    file_name = os.path.join(dir, name)
    with open(file_name, "w") as f:
        f.write(code)
    return file_name

# TODO проверка на ошибки компилирования, удаление файла после работы, настройка прав доступа
def test_strategy(attempt):
    lang = language_class[attempt.language] # получаем по строке класс языка
    algo_name = "robot" + str(attempt.id) # уникализируем название
    source_name = algo_name + lang.extension # склеиваем имя с расширением

    source_path = writeToFolder(SANDBOX, source_name, attempt.code) # пишем код в файл песочницы

    if lang.isCompile:
        compile_path = os.path.join(SANDBOX, algo_name)
        (out, err) = compiler.compile(lang.cmd_compile(source_path, compile_path))
        if err:
            raise CheckError(err)
        else:
            return runner.run_test(lang.cmd_run(compile_path), attempt)
    else:
        return runner.run_test(lang.cmd_run(source_path), attempt)

def main():
    print test_strategy()

if __name__ == '__main__':
    main()
