from django.shortcuts import render
from languages import language_class, C_plusPlus
from sandbox.tasks import run_algo
from models import MarketData
from attempt.models import Attempt
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from contest.models import ContestStock
from django.http import Http404

def sandbox_context():
    context = {
        'marketData': MarketData.objects.all(),
        'languages': language_class,
        'show_mode': True,
    }
    return context

def sandbox_check(code, language, data, user, contest, name=""):
    attempt = Attempt( code=code,
                       language=language,
                       user=user,
                       market_data=data,
                       contest=contest,
                       status='S',
                       error_message='',
                       name=name,
                     )
    attempt.save()
    return run_algo.delay(attempt)

@login_required
def index(request):
    context = sandbox_context()

    try:
        code = request.POST['code']
        language = request.POST['language']
        data_id = request.POST['marketData']

        data = get_object_or_404(MarketData, pk=data_id)
        contest = get_object_or_404(ContestStock, pk=1)

        sandbox_check(code, language, data, request.user, contest)

        context.update({
            'console': "Check problem is running",
            'code': code,
            'error_message': '',
        })
        return render(request, 'sandbox/sandbox.html', context)
    except KeyError:
        code = C_plusPlus.code
        context.update({
            'console': 'Simple example in C++',
            'code': code,
        })
        return render(request, 'sandbox/sandbox.html', context)

@login_required
def show_attempt(request, attempt_id):
	attempt = get_object_or_404(Attempt, pk=attempt_id)
	if request.user == attempt.user:
	    context = {
	        'console': attempt.name,
	        'code': attempt.code,
	        'show_mode': True,
	    }
	    return render(request, 'sandbox/sandbox.html', context)
	else:
		raise Http404("You don't have permission to watch this attempt")