#!/usr/bin/python
#coding=utf-8

from celery import task
from sandbox.check import test_strategy
import traceback
from attempt.models import listBuySell
from celery.utils.log import get_task_logger

logger = get_task_logger('check_log')

@task()
def run_algo(attempt):
    try:
        test_strategy(attempt)
        # listBuySell.objects.bulk_create(events) # с bulk_create есть некоторые проблемы, смотри доки
        attempt.status = 'O'
    except Exception as e:
        attempt.status = 'W'
        attempt.error_message += str(e)
        logger.warning(attempt.error_message)
        attempt.error_message += str(traceback.format_exc())
    finally:
        attempt.save()

# pexpect __init__.py line 493, sys.__stdin__.fileno() doesn't work
@task()
def test(attempt):
    try:
        import sys
        sys.__stdin__.fileno()
    except Exception:
        attempt.error_message = str(traceback.format_exc())
        attempt.save()

# Need periodic function which will kill attempt with status 'S' which ran long time ago