from django.conf.urls import patterns, url
import views

urlpatterns = patterns(
    '',
    url(r'^(?P<attempt_id>[0-9]+)$', views.show_attempt, name='detail'),
    url(r'^$', views.index, name='index'),
)
