class Language():
    name = ""
    extension = ""

    def cmd():
        raise NotImplementedError("should have implemented this on")

class Python2(Language):
    name = "python2"
    full_name = "Python 2.7"
    extension = ".py"
    isCompile = False
    code = """average = 655

print "Start"

while True:
    price = int(raw_input())
    if price < average:
        print "buy 10"
    else:
        print "skip"
"""

    @staticmethod
    def cmd_run(file_name):
        return "python {0}".format(file_name)


class C_plusPlus(Language):
    name = "cpp"
    full_name = "C++"
    extension = ".cpp"
    isCompile = True
    code = """#include <iostream>
using namespace std;

int main() {
    int average = 655;
    float price;

    cout << "Start" << endl;
    while (true) {
        cin >> price;
        if (price < average) {
            cout << "buy 10" << endl;
        } else {
            cout << "skip" << endl;
        }
    }

    return 0;
}
"""

    @staticmethod
    def cmd_compile(file_name, compile_name):
        return "g++ {0} -o {1}".format(file_name, compile_name)

    @staticmethod
    def cmd_run(file_name):
        return "{0}".format(file_name)

language_class = {
    "python2" : Python2,
    "cpp" : C_plusPlus
}
