# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sandbox', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='marketdata',
            name='file_name',
        ),
        migrations.AddField(
            model_name='marketdata',
            name='close_data',
            field=models.FileField(null=True, upload_to=b'closeData', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='marketdata',
            name='open_data',
            field=models.FileField(null=True, upload_to=b'openData', blank=True),
            preserve_default=True,
        ),
    ]
