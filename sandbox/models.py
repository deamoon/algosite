#!/usr/bin/python
#coding=utf-8

from django.db import models
from languages import *
from django.contrib.auth.models import User
import os

SANDBOX = os.environ.get('ALGOSITE_SANDBOX')

class MarketData(models.Model):
    open_data = models.FileField(upload_to='openData', blank=True, null=True)
    close_data = models.FileField(upload_to='closeData', blank=True, null=True)
    description = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def getData(self, type_data):
        if type_data == 'open':
            file_name = self.open_data
        elif type_data == 'close':
            file_name = self.close_data
        try:
            file_name.open('r')
            data = file_name.readlines()
            return data
        finally:
            file_name.close()

    def getOpenData(self):
        return self.getData('open')

    def getCloseData(self):
        return self.getData('close')

    def getPriceData(self, type_data):
        data = self.getData(type_data)
        result = []
        for s in data:
            try:
                result.append(float(s.strip()))
            except:
                pass
        return result
