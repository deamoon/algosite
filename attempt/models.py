#!/usr/bin/python
#coding=utf-8

from django.db import models
from contest.models import ContestStock
from sandbox.models import MarketData
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField

class AttemptResult(models.Model):
    """
    Модель результатов тестирования
    """
    final_portfolio = models.FloatField(blank=True, null=True) # размер портфеля в конце работы
    dispersion = models.FloatField(blank=True, null=True) # дисперсия
    average_return = models.FloatField(blank=True, null=True) # средняя прибыль за тик
    sharp = models.FloatField(blank=True, null=True) # коэфицент Шарпа
    max_drawdown = models.FloatField(blank=True, null=True)
    portfolio = ArrayField(models.FloatField())
    def __unicode__(self):
        return "AttemptResult #" + str(self.id) + " final_portfolio " + str(self.final_portfolio)

class Attempt(models.Model):
    """
    Модель посылки алгоритма. В конце прогона алгоритма, оставшиеся акции продаются
    в дополнительный день по цене последнего тестового дня.
    """
    STATUS_LIST = (
        ('S', 'Sending'),
        ('O', 'OK'),
        ('W', 'Warning')
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST) # статус посылки

    name = models.CharField(max_length=200, default="")
    contest = models.ForeignKey(ContestStock) # id контеста
    market_data = models.ForeignKey(MarketData) # id данных
    user = models.ForeignKey(User) # id пользователя
    code = models.TextField() # текст кода посылки
    error_message = models.TextField(default="") # текст, возможной ошибки
    result_open = models.ForeignKey(AttemptResult, blank=True, null=True, related_name='%(class)s_result_open')
    result_close = models.ForeignKey(AttemptResult, blank=True, null=True, related_name='%(class)s_result_close')

    LANGUAGE_LIST = (
        ('python2', 'Python 2.7'),
        ('cpp', 'G++ 4.6.3')
    )
    language = models.CharField(max_length=20, choices=LANGUAGE_LIST) # язык кода посылки

    def _fillList(self, array):
        """
        Заполняет пропуски в массиве данными, предудущего заполненного элемента.
        Гарантируется, что в массиве есть минимальный номер 0 и максимальный.
        Пример _fillList([(0, data0), (4, data4), (2, data2)]) ==
                         [(0, data0), (1, data0), (2, data2), (3, data2), (4, data4)]
        """
        max_number = max([a[0] for a in array])
        result = [None] * (max_number + 1)
        for a in array:
            result[a[0]] = a

        for num, a in enumerate(result):
            if a is None:
                result[num] = tuple([num] + list(result[num-1])[1:])
        return result

    def getListEvents(self, allEvents=False):
        """
        Вернем список вида [("buy", 0, 12.0), ]
        """
        buy_sell = listBuySell.objects.filter(attempt__id=self.id)
        return [(d.order, d.number, d.quantity) for d in buy_sell if not d.error or allEvents]

    def getListBalance(self):
        """
        В работе runner.py добавляется доп. день для продажи акций.
        И здесь ещё все дни смещаются на день вперед, для отображения графиков
        """
        buy_sell = listBuySell.objects.filter(attempt__id=self.id)
        orders = [(0, self.contest.start_balance_money, self.contest.start_balance_stock), ]
        for d in buy_sell:
            # d.number+1, так как результат сделки на графиках виден на следующий тик
            orders.append((d.number+1, d.current_balance_money, d.current_balance_stock))
        return self._fillList(orders)

    def __unicode__(self):
        return self.contest.name + " attempt #" + str(self.id) + " status " + self.status

class listBuySell(models.Model):
    """
    Модель списка событий, типа купил, продал фиксированное кол-во акций.
    Неформальным ключом является пара (attempt, number). Возможно только одно событие за тик.
    Тики, в которые не совершается действий, не хранятся.
    """
    ORDER_LIST = (
        ('buy', 'Buy'),
        ('sell', 'Sell'),
    )
    order = models.CharField(max_length=20, choices=ORDER_LIST) # тип ордера
    quantity = models.FloatField() # количество
    number = models.IntegerField() # номер даты в данных, обобщение даты, ключ
    attempt = models.ForeignKey(Attempt) # id посылки, ключ
    attempt_number = models.CharField(max_length=20, primary_key=True) # хитрожопый ключ
    current_balance_money = models.FloatField() # деньги
    current_balance_stock = models.FloatField() # акции
    error = models.TextField(default="") # текст, возможной ошибки

    def __unicode__(self):
        return self.order + " " + str(self.quantity)
