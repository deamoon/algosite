#!/usr/bin/python
#coding=utf-8

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from models import Attempt, listBuySell
import json
from django.http import HttpResponse

def detail(request, attempt_id):
    attempt = get_object_or_404(Attempt, pk=attempt_id)
    context = {
        'buy_sell': listBuySell.objects.filter(attempt__id=attempt_id),
        'attempt': attempt,
    }
    return render(request, 'attempt/detail.html', context)

def data(request, attempt_id):
    attempt = get_object_or_404(Attempt, pk=attempt_id)
    # orders = attempt.getListEvents()
    priceData = attempt.market_data.getPriceData('open')
    # priceData.append(priceData[-1]) # Добавляем доп день
    # balance_list = attempt.getListBalance()
    jsonDict = {"price": priceData,
                "portfolio": attempt.result_open.portfolio,
                # "title_events": ("order", "number", "quantity"),
                # "events": orders,
                # "title_balance": ("number", "money", "stock"),
                # "balance": balance_list,
               }
    return HttpResponse(json.dumps(jsonDict), content_type="application/json")


