# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attempt', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attempt',
            name='language',
            field=models.CharField(max_length=20, choices=[(b'python2', b'Python 2.7'), (b'cpp', b'G++ 4.6.3')]),
        ),
    ]
