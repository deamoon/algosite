# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('attempt', '0003_attempt_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttemptResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('final_portfolio', models.FloatField(null=True, blank=True)),
                ('dispersion', models.FloatField(null=True, blank=True)),
                ('average_return', models.FloatField(null=True, blank=True)),
                ('sharp', models.FloatField(null=True, blank=True)),
                ('max_drawdown', models.FloatField(null=True, blank=True)),
                ('portfolio', django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), size=None)),
            ],
        ),
        migrations.RemoveField(
            model_name='attempt',
            name='final_balance_money',
        ),
        migrations.AddField(
            model_name='attempt',
            name='result_close',
            field=models.ForeignKey(related_name='attempt_result_close', blank=True, to='attempt.AttemptResult', null=True),
        ),
        migrations.AddField(
            model_name='attempt',
            name='result_open',
            field=models.ForeignKey(related_name='attempt_result_open', blank=True, to='attempt.AttemptResult', null=True),
        ),
    ]
