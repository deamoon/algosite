# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0001_initial'),
        ('sandbox', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attempt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=20, choices=[(b'S', b'Sending'), (b'O', b'OK'), (b'W', b'Warning')])),
                ('code', models.TextField()),
                ('error_message', models.TextField(default=b'')),
                ('final_balance_money', models.FloatField(null=True, blank=True)),
                ('language', models.CharField(max_length=20, choices=[(b'python2', b'Python 2.7'), (b'c++', b'G++ 4.6.3')])),
                ('contest', models.ForeignKey(to='contest.ContestStock')),
                ('market_data', models.ForeignKey(to='sandbox.MarketData')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='listBuySell',
            fields=[
                ('order', models.CharField(max_length=20, choices=[(b'buy', b'Buy'), (b'sell', b'Sell')])),
                ('quantity', models.FloatField()),
                ('number', models.IntegerField()),
                ('attempt_number', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('current_balance_money', models.FloatField()),
                ('current_balance_stock', models.FloatField()),
                ('error', models.TextField(default=b'')),
                ('attempt', models.ForeignKey(to='attempt.Attempt')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
