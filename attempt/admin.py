from django.contrib import admin
from models import Attempt, listBuySell, AttemptResult

admin.site.register(Attempt)
admin.site.register(listBuySell)
admin.site.register(AttemptResult)
