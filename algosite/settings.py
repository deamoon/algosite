# -*- coding: utf-8 -*-

"""
Django settings for algosite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-j=sirtird*urd!k81dv1-@lzp&ms4yj8g5h(cc_=q2=o8=%yq'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
# DEBUG = True

SITE_ID = 1
ALLOWED_HOSTS = ['localhost', '.quantforces.com', '.bcsquants.com']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'sandbox',
    'my_registration', # django-registration
    'crispy_forms', # django-crispy-forms
    'user_profile',
    'contest',
    'attempt',
    # 'djcelery',
    # 'djkombu',
    'staticpage',
    'django_wysiwyg',
    'tinymce',
    'blog',
    'data_upload',
    'bcs_client',

    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'rest_auth.registration',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    # 'allauth.socialaccount.providers.github',
    # 'allauth.socialaccount.providers.google',
    # 'allauth.socialaccount.providers.vk',

    'tradingview',

    'easy_thumbnails',
    'filer',
    'mptt',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'algosite.urls'

WSGI_APPLICATION = 'algosite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('BD_ALGOSITE_DATABASE_NAME'), # 'algosite'
        'USER': os.environ.get('BD_USER'), # 'root'
        'PASSWORD': os.environ.get('BD_PASSWORD'), # '1'
        'HOST': os.environ.get('BD_HOST'), # localhost
        'PORT': os.environ.get('BD_PORT'), # 5432
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Europe/Moscow'

# USE_I18N = True

USE_L10N = False

# USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_MEDIA = '/media/'
STATIC_ROOT = os.environ.get('ALGOSITE_STATIC')
MEDIA_ROOT = os.environ.get('ALGOSITE_MEDIA')
ALGOSITE_SANDBOX = os.environ.get('ALGOSITE_SANDBOX')
ALGOSITE_DATABOX = os.environ.get('ALGOSITE_DATABOX')

DJANGO_WYSIWYG_FLAVOR = 'tinymce_advanced'

### DJANGO-REGISTRATION

ACCOUNT_ACTIVATION_DAYS = 2 # кол-во дней для хранения кода активации

# для отправки кода активации
AUTH_USER_EMAIL_UNIQUE = True
EMAIL_HOST = 'mail.bcsquants.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'info@bcsquants.com'
EMAIL_HOST_PASSWORD = '3FYS9g25u76WxRT'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'info@bcsquants.com'

### DJANGO-REGISTRATION

### TEMPLATE

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates').replace('\\','/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                # `allauth` needs this from django
                'django.template.context_processors.request',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)

SOCIALACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
SOCIALACCOUNT_AUTO_SIGNUP = True
ACCOUNT_USERNAME_REQUIRED = False

# import djcelery
# djcelery.setup_loader()
# CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
# BROKER_BACKEND = "djkombu.transport.DatabaseTransport"

# CELERY STUFF
BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'
CELERY_SEND_TASK_ERROR_EMAILS = True
CELERY_DISABLE_RATE_LIMITS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(message)s',
            'datefmt': '%y %b %d, %H:%M:%S',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'celery_log': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'celery.log',
            'formatter': 'simple',
            'maxBytes': 1024 * 1024 * 1,  # 100 mb
        },
        'django_log': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'django.log',
            'formatter': 'simple',
            'maxBytes': 1024 * 1024 * 1,  # 100 mb
        },
    },
    'loggers': {
        'check_log': {
            'handlers': ['celery_log', 'console', 'mail_admins'],
            'level': 'INFO',
        },
        'celery': {
            'handlers': ['celery_log', 'console'],
            'level': 'INFO',
        },
        'django': {
            'handlers': ['mail_admins', 'django_log'],
            'level': 'INFO',
        }
    }
}

import logging.config
logging.config.dictConfig(LOGGING)

LOGIN_REDIRECT_URL = "/main"

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    # 'DEFAULT_PERMISSION_CLASSES': [
    #     'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    # ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'PAGE_SIZE': 20,
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',)
}

BCS_QUANTS_CHALLENGE = {
    'numberOnlineAttempt': 10,
}

ACCOUNT_SIGNUP_FORM_CLASS = 'bcs_client.forms.SignupForm'

THUMBNAIL_HIGH_RESOLUTION = True
FILER_CANONICAL_URL = 'sharing/'

from data_upload.data_manage import universary4
UNIVERSARY = universary4

try:
    from local_settings import *
except ImportError:
    pass
