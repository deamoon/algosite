# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from bcs_client.models import StrategyBcs, AttemptBcs, Profile, AttemptResultBcs
from rest_framework import routers, serializers, viewsets, permissions
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView

from allauth.exceptions import ImmediateHttpResponse
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter

class IsOwnerReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        attempt = getattr(obj, 'attempt', None)
        if attempt:
            user = obj.attempt.user
        else:
            user = obj.user
        return user == request.user

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Profile
        read_only_fields = ('strategys', 'rating', 'user')
        # extra_kwargs = {'password': {'write_only': True},
        #                 'email': {'write_only': True}}

    # def create(self, validated_data):
    #     user = User(
    #         email=validated_data['email'],
    #         username=validated_data['username']
    #     )
    #     user.set_password(validated_data['password'])
    #     user.save()

    #     userBcsData = validated_data.pop('userBcs')
    #     UserBcs.objects.create(user=user, **userBcsData)

    #     return user

class StrategyBcsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = StrategyBcs
        fields = ('pk', 'name', 'bAttempt', 'oAttempt', 'oTicker', 'mode', 'onlineDatetime', 'attempts')
        read_only_fields = ('bAttempt', 'oAttempt', 'oTicker', 'onlineDatetime', 'attempts')

class AttemptBcsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AttemptBcs
        fields = ('pk', 'strategy', 'code', 'name', 'status', 'mode', 'error_message', 'controlResult', 'numDeals', 'sumProcent', 'attemptResults')
        read_only_fields = ('status', 'error_message', 'mode', 'controlResult', 'numDeals', 'sumProcent', 'attemptResults')

    def create(self, validated_data):
        validated_data['language'] = 'python3'
        attempt = AttemptBcs.objects.create(**validated_data)
        attempt.send()
        return attempt

class AttemptResultBcsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AttemptResultBcs
        fields = ('ticker', 'jsonResult')
        read_only_fields = ('ticker', 'jsonResult')

class ProfileViewSet(viewsets.ModelViewSet):
    """
    *Profiles* connected with [User][Users].

    *Profile* has a *rating*.

    Order of strategys is always from last to old.

    [Users]: /rest-auth/user
    [Profiles]: /rest-api/profiles
    [attempts]: /rest-api/attempts
    [strategy]: /rest-api/strategys
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    # def perform_create(self, serializer):
    #     UserBcs(user=serializer.save()).save()

class StrategyBcsViewSet(viewsets.ModelViewSet):
    """
    [Users][Users] create strategys.
    Strategy represents an idea of what trading algorithms can be.
    The final implementation of idea is attempt.
    Strategy accumulates [attempts][attempts] and help to navigate through attempts.

    To create strategy you only need a name. Name can be empty string.
    There are 2 modes for strategy. **O** - online and **B** - backtest.
    Strategy always created in **B** mode.
    In **B** mode user can send attempts and last attempt will update bAttempt.
    User can update strategy mode from **B** to **O**
    if he has a bAttempt with status **O**. After updating mode to **O** User can't send new attempts and change mode.
    Online strategy is tested every day on a new day and oAttempt results is updated.
    To send strategy to online User must specify oTicker from special list of tickers.

    Screen of strategy should has form for sending new attempt and show *current* attempt results.
    If strategy in backtest mode you should show bAttempt as *current* attempt,
    for online mode show oAttempt  as *current* attempt.

    Datetime is always in Moscow zone.
    Order of attempts is always from last to old.

    [Users]: /rest-auth/user
    [Profiles]: /rest-api/profiles
    [attempts]: /rest-api/attempts
    """
    serializer_class = StrategyBcsSerializer
    permission_classes = (IsOwnerReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        # if user.is_staff:
        #     return StrategyBcs.objects.all()
        return StrategyBcs.objects.filter(user=user)

class AttemptBcsViewSet(viewsets.ModelViewSet):
    """
    [Profiles][Profiles] create attempts inside [strategy][strategy].
    Attempt is an implementation of idea in a *code*.

    Every new attempt is sent to one of the backtest server where it's tested on history market data.
    It can be a long process. Attempt accumulates [attemptResults].
    Attempt can have 3 status values, **S** - sending, **O** - ok, **E** - error.
    While backtest server is testing new attempts attempt has a **S** status.
    Some *attemptResults* will appear before others, you should show all *attemptResults*.
    *attemptResult* is a row in a table of attempt. A lot of attempts will finish before the end because of programming errors. Attempt status will updated on **E**.
    In this case show *error_message* and don't show *attemptResults*. If status updated on **O**. Attempt was fully tested.

    Attempt also as strategy has mode, **O** - online and **B** - backtest.
    Attempt with status Ok has rating results. For *online* attempt results are *numDeals*, *sumProcent*
    and for *backtest* attempt results are *numDeals*, *sumProcent*, *controlResult*

    Order of attemptResult is always from old to new.

    Do post request to **/rest-api/attempts/id/send_online** to change mode to **O**

    [Profiles]: /rest-api/profiles
    [strategy]: /rest-api/strategys
    [attemptResults]: /rest-api/attemptResults
    """
    serializer_class = AttemptBcsSerializer
    permission_classes = (IsOwnerReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        # if user.is_staff:
        #     return AttemptBcs.objects.all()
        return AttemptBcs.objects.filter(user=user)

    @detail_route(methods=['get'], permission_classes=[IsOwnerReadOnly])
    def send_online(self, request, pk=None):
        attempt = AttemptBcs.objects.get(pk=pk)
        if request.user.id != attempt.user.id:
            return Response({'result': 'error', 'error_message': 'User must be owner of the attempt'}, status=403)

        if attempt.status == 'O':
            attempt.sendOnline()
            return Response({'result': 'ok', 'error_message': ''})
        else:
            return Response({'result': 'error', 'error_message': 'Попытка должна иметь статус OK'})

class AttemptResultBcsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    *AttemptResult* is only for reading.
    *AttemptResult* has *ticker*. Ticker is a name of market instrument.
    *AttemptResult* has jsonResult. This field is not fixed. The keys can change in a future.

    [Profiles]: /rest-api/profiles
    [strategy]: /rest-api/strategys
    [attemptResults]: /rest-api/attemptResults
    """

    serializer_class = AttemptResultBcsSerializer
    permission_classes = (IsOwnerReadOnly,)

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return AttemptResultBcs.objects.all()
        return AttemptResultBcs.objects.filter(attempt__user=user)


router = routers.DefaultRouter(schema_title='Quantforces API')
router.register(r'profiles', ProfileViewSet, 'profile')
router.register(r'strategys', StrategyBcsViewSet, 'strategybcs')
router.register(r'attempts', AttemptBcsViewSet, 'attemptbcs')
router.register(r'attemptResults', AttemptResultBcsViewSet, 'attemptresultbcs')

###############################################################################################
from django.conf.urls import patterns, url, include
from django.contrib import admin
from allauth.account.views import ConfirmEmailView

admin.autodiscover()

urlpatterns = patterns(
    '',
    # url(r'^', include('staticpage.urls', namespace="staticpage")),
    # url(r'^sandbox/', include('sandbox.urls', namespace="sandbox")),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^accounts/', include('my_registration.backends.default.urls')), # django-registration
    # url(r'^contest/', include('contest.urls', namespace="contest")),
    # url(r'^attempt/', include('attempt.urls', namespace="attempt")),
    # url(r'^blog/', include('blog.urls', namespace="blog")),
    # url(r'^i18n/', include('django.conf.urls.i18n')),
    # url(r'^data_upload/', include('data_upload.urls', namespace="data_upload")),
    url(r'^', include('bcs_client.urls', namespace="bcs_client")),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/', include('user_profile.urls')),

    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^rest-api/', include(router.urls)),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$',
        ConfirmEmailView.as_view(), name="account_confirm_email"),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    # url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),

    url(r'^filer/', include('filer.urls')),

    # url(r'^tradingview/', include('tradingview.urls', namespace='tradingview')),
)
