# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from algosite.settings import UNIVERSARY
import time
import requests
import datetime
import calendar
from bcs_client.models import AttemptBcs

@login_required
def index(request):
    context = {}
    return render(request, 'tradingview/index.html', context)

def config(request, attempt_id):
    return JsonResponse({
        'supports_search': False,
        'supports_group_request': True,
        "supported_resolutions": ["1", "5", "10", "30", "60", "240", "1D"],
        # ticks/s1/s5/m1/m5/m10/m30/h1/h4/d1
        'supports_marks': True,
        'supports_time': True,
        'exchanges': [
            # {value: "", name: "All Exchanges", desc: ""},
            # {value: "XETRA", name: "XETRA", desc: "XETRA"},
            {'value': "MOEX", 'name': "MOEX", 'desc': "Московская биржа"}
        ],
        'symbolsTypes': [
            # {name: "All types", value: ""},
            {'name': "Stock", 'value': "stock"},
            # {name: "Index", value: "index"}
        ],
    })

def symbol_info(request, attempt_id):
    # group = request.GET['group']

    symbols = ['SBER', 'VTBR', 'LKOH', 'ALRS', 'MGNT', 'SBERP', 'ROSN', 'MOEX', 'GAZP',
               'SNGS', 'USD000UTSTOM', 'RTSI', 'MICEXINDEXCF', 'GZX', 'SIX', 'BRX']
    descriptions = ['Сбербанк', 'ВТБ', 'Лукойл', 'Алроса', 'Магнит', 'Сбербанк привилегированные', 'Роснефть', 'Московская биржа', 'Газпром',
                   'Сургутнефтегаз', 'Доллар/Рубль', 'Индекс РТС', 'Индекс ММВБ', 'Золото фьючерс', 'Доллар фьючерс', 'Нефть фьючерс']
    minmovs = [1, 1, 5, 1, 1, 1, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    pricescales = [100, 100000, 10, 100, 1, 100, 100, 100, 100, 100, 1000, 100, 100, 1, 1, 100]

    symbols = symbols + ['#' + x for x in symbols]
    descriptions = descriptions + ['#' + x for x in descriptions]
    minmovs *= 2
    pricescales *= 2

    return JsonResponse({
        "symbol": symbols,
        "description": descriptions,

        "timezone": "Europe/Moscow",

        "minmov": minmovs,
        "minmov2": 0,
        "pricescale": pricescales,

        "session-regular": "1000-1840",
        "has-empty-bars": True,

        "has-weekly-and-monthly": False,
        "has-daily": True,
        "has-intraday": True,
        "has-seconds": False,
        "has-no-volume": False,

        # "supported-resolutions": ["1S", "5S", "1", "5", "10", "30", "60", "240", "1D"],
        "supported-resolutions": [["1", "5", "10", "30", "60", "240", "1D"] for _ in symbols],
        "intraday-multipliers": [["1", "5", "10", "30", "60", "240"] for _ in symbols], # for minutes
        # "seconds-multipliers": ["1", "5"],

        # "pointvalue": 1,
        "type": "stock",
        "exchange-traded": "MOEX",
        "exchange-listed": "MOEX",
    })

def search(request, attempt_id):
    pass

def getTime(request, attempt_id):
    return HttpResponse(str(int(time.time())))

resolutionToNewFormat = {
    '1S': 's1', '5S': 's5', 'S': 's1',
    '1': 'm1', '5': 'm5', '10': 'm10', '30': 'm30',
    '60': 'h1', '240': 'h4',
    'D': 'd1', '1D': 'd1',
}

symbolToInfo = {
    elem['symbol']:{
        'board': elem['board'],
        'provider': elem['provider']
    } for elem in UNIVERSARY['instruments']
}

def converMoscowServerDatetimeToUtcTimestamp(dt):
    return calendar.timegm((dt - datetime.timedelta(hours=3)).timetuple())

def getTrades(attempt, symbol):
    r = requests.get('https://bcsquants.com/{0}/attempt/{1}/order/{2}_order.csv'.format(attempt.worker.name, attempt.workerAttemptId, symbol))
    res = r.text
    data = res.split('\n')[1:]
    result = []
    for ind, line in enumerate(data):
        if not line:
            continue
        lineSplit = line.strip().split(';')
        datetimeEnter, direct, priceEnter, procent, event, datetimeExit, priceClose, startSumProcent, endSumProcent = lineSplit[:9]
        datetimeEnter, direct, priceEnter, procent, event, datetimeExit, priceClose, startSumProcent, endSumProcent = \
        dtc(datetimeEnter), int(direct), float(priceEnter), float(procent), int(event), dtc(datetimeExit.strip()), \
        float(priceClose), float(startSumProcent), float(endSumProcent)
        result.append((datetimeEnter, direct, priceEnter, procent, event, datetimeExit, priceClose, startSumProcent, endSumProcent))
    return result

def history(request, attempt_id):
    attempt = AttemptBcs.objects.get(id=attempt_id)
    # if not request.user.is_staff and request.user.id != attempt.user.id:
    #     return HttpResponse(status=400)

    def getServerHistory(symbol, resolution, fromDatetime, toDatetime):
        isSumProcent = ('#' in symbol)
        if isSumProcent:
            symbol = symbol[1:]

        timeList, closeList, openList, highList, lowList, volumeList = [], [], [], [], [], []
        result = {
           's': "ok",
           't': timeList,
           'c': closeList,
           'o': openList,
           'h': highList,
           'l': lowList,
           'v': volumeList,
        }

        firstTestDatetime = datetime.datetime.strptime(UNIVERSARY['firstTestDatetime'], "%Y-%m-%d %H:%M:%S")
        lastTestDatetime = datetime.datetime.strptime(UNIVERSARY['lastTestDatetime'], "%Y-%m-%d %H:%M:%S")

        if attempt.mode == 'B':
            if fromDatetime > lastTestDatetime:
                return {
                    's': 'no_data',
                    'nextTime': converMoscowServerDatetimeToUtcTimestamp(lastTestDatetime),
                }
            elif toDatetime < firstTestDatetime:
                return {
                    's': 'no_data'
                }
            else:
                fromDatetime = max(fromDatetime, firstTestDatetime)
                toDatetime = min(toDatetime, lastTestDatetime)
        else:
            onlineDatetime = attempt.onlineDatetime
            if toDatetime < onlineDatetime:
                return {
                    's': 'no_data'
                }
            else:
                fromDatetime = max(fromDatetime, onlineDatetime)
                # toDatetime = min(toDatetime, lastTestDatetime)

        try:
            r = requests.get('http://89.249.27.203:50011/papers/{provider}/{board}/{symbol}/{fromDate}-{toDate}/{resolution}/csv'.format(
                symbol=symbol, resolution=resolution,
                provider=symbolToInfo[symbol]['provider'], board=symbolToInfo[symbol]['board'],
                fromDate=fromDatetime.strftime('%Y%m%d'), toDate=toDatetime.strftime('%Y%m%d')))
            res = r.text
            data = res.split('\n')[1:]

            sumNumber = 0
            for line in data:
                timeBar, openBar, high, low, close, volume, number = line.split(';')[:7]
                sumNumber += int(number)

            dtTimeList = []
            if sumNumber > 0:
                for line in data:
                    timeBar, openBar, high, low, close, volume, number = line.split(';')[:7]
                    dt = datetime.datetime.strptime(timeBar, "%d.%m.%Y %H:%M:%S")
                    if int(number) > 0 and fromDatetime <= dt <= toDatetime:
                        timeList.append(converMoscowServerDatetimeToUtcTimestamp(dt))
                        closeList.append(float(close))
                        if isSumProcent:
                            dtTimeList.append(dt)
                        else:
                            openList.append(float(openBar))
                            highList.append(float(high))
                            lowList.append(float(low))
                            volumeList.append(int(volume))

            if isSumProcent:
                trades = getTrades(attempt, symbol)
                # datetimeEnter, direct, priceEnter, procent, event, datetimeExit, priceClose, startSumProcent, endSumProcent = trades

                if trades and dtTimeList:
                    dt = dtTimeList[0]
                    if dt >= trades[-1][5]:
                        closeList = [trades[-1][8]] * len(closeList) # endSumProcent
                    else:
                        if dt < trades[-1][0]:
                            inIntervalDate = False
                            prTradeProcent = 0.0
                            tradesInd = 0
                        else:
                            for tradesInd in range(len(trades)):
                                datetimeEnter, datetimeExit = trades[tradesInd][0], trades[tradesInd][5]
                                startSumProcent, endSumProcent = trades[tradesInd][7], trades[tradesInd][8]
                                priceEnter, priceClose = trades[tradesInd][2], trades[tradesInd][6]
                                direct = trades[tradesInd][1]
                                lastTrade = (tradesInd == len(trades) - 1)
                                if (datetimeEnter <= dt < datetimeExit):
                                    prTradeProcent = startSumProcent
                                    prPrice = priceEnter
                                    inIntervalDate = True
                                    break
                                elif (not lastTrade and datetimeEnter <= dt < trades[tradesInd+1][0]):
                                    prTradeProcent = endSumProcent
                                    prPrice = priceClose
                                    inIntervalDate = False
                                    break

                        newCloseList = []
                        for dt, close in zip(dtTimeList, closeList):
                            datetimeEnter, datetimeExit = trades[tradesInd][0], trades[tradesInd][5]
                            startSumProcent, endSumProcent = trades[tradesInd][7], trades[tradesInd][8]
                            priceEnter, priceClose = trades[tradesInd][2], trades[tradesInd][6]
                            direct = trades[tradesInd][1]
                            lastTrade = (tradesInd == len(trades) - 1)
                            if inIntervalDate and dt >= datetimeExit:
                                inIntervalDate = False
                                prTradeProcent = endSumProcent
                                # prPrice = priceClose
                            elif not inIntervalDate and (not lastTrade and dt >= trades[tradesInd + 1][0]): # datetimeEnter
                                inIntervalDate = True
                                prTradeProcent = endSumProcent
                                prPrice = trades[tradesInd + 1][2] # priceEnter
                                tradesInd += 1

                            if inIntervalDate:
                                procent = prTradeProcent + (close - prPrice) / prPrice * direct
                                newCloseList.append(procent)
                            else:
                                newCloseList.append(prTradeProcent)
                        closeList = newCloseList
                else:
                    closeList = [0] * len(closeList)
                result['c'] = [x * 100 for x in closeList]

            if not timeList:
                result['s'] = 'no_data'
                for i in range(5):
                    toDatetime = fromDatetime
                    fromDatetime = toDatetime - datetime.timedelta(days=3)
                    r = requests.get('http://89.249.27.203:50011/papers/{provider}/{board}/{symbol}/{fromDate}-{toDate}/{resolution}/csv'.format(
                        symbol=symbol, resolution=resolution,
                        provider=symbolToInfo[symbol]['provider'], board=symbolToInfo[symbol]['board'],
                        fromDate=fromDatetime.strftime('%Y%m%d'), toDate=toDatetime.strftime('%Y%m%d')))
                    res = r.text
                    data = res.split('\n')[1:][::-1]
                    for line in data:
                        timeBar, openBar, high, low, close, volume, number = line.split(';')[:7]
                        if number > 0:
                            dt = datetime.datetime.strptime(timeBar, "%d.%m.%Y %H:%M:%S")
                            if datetime.time(hour=10) <= dt.time() <= datetime.time(hour=18, minute=40):
                                result['nextTime'] = converMoscowServerDatetimeToUtcTimestamp(dt)
                                return result

        except Exception as e:
            result['s'] = 'error'
            import traceback
            result['exception'] = traceback.format_exc()
            # raise e

        return result

    symbol = request.GET['symbol']
    resolution = resolutionToNewFormat[request.GET['resolution']]
    fromDatetime = datetime.datetime.utcfromtimestamp(int(request.GET['from'])) + datetime.timedelta(hours=3)
    toDatetime = datetime.datetime.utcfromtimestamp(int(request.GET['to'])) + datetime.timedelta(hours=3)
    return JsonResponse(getServerHistory(symbol, resolution, fromDatetime, toDatetime))

def dtc(dt):
    return datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")

def marks(request, attempt_id):
    attempt = AttemptBcs.objects.get(id=attempt_id)
    def getServerMarks(symbol, resolution, fromDatetime, toDatetime):
        result = {
            'id': [], 'time': [], 'color': [], 'text': [], 'label': [],
            # 'labelFontColor': [],
            'minSize': 20
        }
        eventToLabel = {
            1: 'P', 0: 'H', -1: 'L'
        }
        # eventToColor = {
        #     1: 'green', 0: 'orange', -1: 'red'
        # }
        directToLabel = {
            1: 'B', -1: 'S'
        }
        directToColor = {
            1: 'yellow', -1: 'yellow'
        }
        directToText = {
            1: 'Покупка', -1: 'Продажа'
        }
        procentToText = lambda x: 'Прибыль' if x > 0 else 'Убытки'
        executeDatetime = set()

        r = requests.get('https://bcsquants.com/{0}/attempt/{1}/order/{2}_order.csv'.format(attempt.worker.name, attempt.workerAttemptId, symbol))
        res = r.text
        data = res.split('\n')[1:]
        for ind, line in enumerate(data):
            if not line:
                continue
            lineSplit = line.strip().split(';')
            datetimeEnter, direct, priceEnter, procent, event, datetimeExit = lineSplit[:6]
            datetimeEnter, direct, priceEnter, procent, event, datetimeExit = \
            dtc(datetimeEnter), int(direct), float(priceEnter), float(procent), int(event), dtc(datetimeExit.strip())
            if len(lineSplit) > 6:
                priceClose = float(lineSplit[6])
            else:
                priceClose = priceEnter * (1 + direct * procent)

            if fromDatetime <= datetimeEnter <= toDatetime:
                executeDatetime.add(datetimeEnter)
                result['time'].append(converMoscowServerDatetimeToUtcTimestamp(datetimeEnter))
                result['color'].append(directToColor[direct])
                result['label'].append(directToLabel[direct])
                result['id'].append(2 * ind)
                result['text'].append('{0} по {1}'.format(directToText[direct], priceEnter))
                # result['labelFontColor'].append(directToColor[direct])

            if fromDatetime <= datetimeExit <= toDatetime:
                result['time'].append(converMoscowServerDatetimeToUtcTimestamp(datetimeExit))
                result['color'].append('green' if procent > 0 else 'red')
                result['label'].append(eventToLabel[event])
                result['id'].append(2 * ind + 1)
                result['text'].append('{0} по {1} </br> {2} {3}%'.format(directToText[-direct], priceClose,
                                                                        procentToText(procent), round(procent * 100, 2)))


        # r = request.get('https://bcsquants.com/backtest2/attempt/972/order/RIX_tick.csv')
        # res = r.text
        # data = res.split('\n')[1:]
        # for line in data:
        #     direct, takeProfit, stopLoss, holdPeriod, datetime = line.split(';')[:5]
        #     result['color'].append()

        return result

    symbol = request.GET['symbol']
    resolution = resolutionToNewFormat[request.GET['resolution']]
    fromDatetime = datetime.datetime.utcfromtimestamp(int(request.GET['from'])) + datetime.timedelta(hours=3)
    toDatetime = datetime.datetime.utcfromtimestamp(int(request.GET['to'])) + datetime.timedelta(hours=3)
    return JsonResponse(getServerMarks(symbol, resolution, fromDatetime, toDatetime))