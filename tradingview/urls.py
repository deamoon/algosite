from django.conf.urls import patterns, url
from tradingview import views

urlpatterns = patterns(
    '',
    url(r'^config/', views.config, name='config'),
    url(r'^symbol_info/', views.symbol_info, name='symbol_info'),
    url(r'^time/', views.getTime, name='time'),
    url(r'^history/', views.history, name='history'),
    url(r'^marks/', views.marks, name='marks'),
    url(r'^search/', views.search, name='search'),
    url(r'^$', views.index, name='index'),
)
