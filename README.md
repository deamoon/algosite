backtest2:
    ssh qf1 -p 3002

    server:
        qf1:4002/admin
        deamoon:diman95qf

/etc/hosts
    92.242.44.43 qf1
    92.242.44.43 bcsquants.com

Create Package:
    https://packaging.python.org/distributing/

Update repo on backtest:
    cd algosite/ansible/backTest
    ./run.sh

Update data in doc:
    cd algosite/data_upload
    python data_download.py PASSWORD