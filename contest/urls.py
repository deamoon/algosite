from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
    url(r'^$', views.contest_main, name='contest_main'),
    url(r'^(?P<contest_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<contest_id>[0-9]+)/poll_state/$', views.poll_state, name='poll_state'),
)