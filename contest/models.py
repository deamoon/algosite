#!/usr/bin/python
#coding=utf-8

from django.db import models

class Problem(models.Model):
    """
    Абстрактная модель задачи, используется для хранения важной технической информации,
    которая необходима для тестирующей системы
    """
    name = models.CharField(max_length=200) # имя задачи
    description = models.TextField() # описание задачи
    time_limit = models.FloatField() # ограничение по общему времени выполнения
    time_step_limit = models.FloatField() # ограничение по времени тика
    memory_limit = models.FloatField() # ограничение по памяти

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name

class ContestStock(Problem):
    """
    Модель контеста валюта/акции, идеально подходит для соревнований по торговле криптовалютой
    """
    start_time = models.DateTimeField() # начало контеста
    end_time = models.DateTimeField() # окончание контеста
    start_balance_money = models.FloatField() # валюта на старте
    start_balance_stock = models.FloatField(default=0.0) # акции на старте
    comission = models.FloatField(default=0.0) # комиссия за сделку в процентах
    marketData = models.ForeignKey('sandbox.MarketData') # данные
