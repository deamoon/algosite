from django.contrib import admin
from models import ContestStock

class ContestStockAdmin(admin.ModelAdmin):
    change_form_template = 'contest/admin/change_form.html'

admin.site.register(ContestStock, ContestStockAdmin)