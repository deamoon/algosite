# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sandbox', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContestStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('time_limit', models.FloatField()),
                ('time_step_limit', models.FloatField()),
                ('memory_limit', models.FloatField()),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('start_balance_money', models.FloatField()),
                ('start_balance_stock', models.FloatField(default=0.0)),
                ('comission', models.FloatField(default=0.0)),
                ('marketData', models.ForeignKey(to='sandbox.MarketData')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
