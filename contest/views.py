from django.shortcuts import render
from models import ContestStock
from django.shortcuts import get_object_or_404
from attempt.models import Attempt
from sandbox.views import sandbox_check
from sandbox.languages import language_class
from sandbox.models import MarketData
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from celery.result import AsyncResult

def contest_main(request):
    contests = ContestStock.objects.all()
    context = {
        'contests': contests
    }
    return render(request, 'contest/contest.html', context)

def detail(request, contest_id):
    contest = get_object_or_404(ContestStock, pk=contest_id)
    data = contest.marketData
    context = {
        'attempts': Attempt.objects.filter(contest__id=contest_id,
                                           user__id=request.user.id).order_by('-id'),
        'contest': contest,
        'marketData': [contest.marketData, ],
        'languages': language_class,
        'console': '',
        'code': '',
        'error_message': '',
        'show_mode': False,
    }

    if 'task_id' in request.session.keys() and request.session['task_id']:
        context.update({
            'task_id': request.session['task_id'],
        })

    try:
        code = request.POST['code']
        name = request.POST['name']
        language = request.POST['language']

        job = sandbox_check(code, language, data, request.user, contest, name)
        request.session['task_id'] = job.id

        context.update({
            'console': "Your algo is running",
            'task_id': job.id,
        })
    except KeyError:
        context.update({
            'console' : "Just send algo",
        })

    return render(request, 'contest/detail.html', context)

@csrf_exempt
def poll_state(request, contest_id):
    """ Get state of the task """
    task_info = 'Fail'
    if request.is_ajax():
        if 'task_id' in request.POST.keys() and request.POST['task_id']:
            task_id = request.POST['task_id']
            task = AsyncResult(task_id)
            task_info = task.result or task.state
        else:
            task_info = 'No task_id in the request'
    else:
        task_info = 'This is not an ajax request'
    json_data = json.dumps(task_info)
    return HttpResponse(json_data, content_type='application/json')
