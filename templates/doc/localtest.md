# Локальное тестирование

Мы настоятельно рекомендуем освоить локальное тестирование ваших стратегий, это позволит вам значительно ускорить поиск и проверку ваших идей. Даже если вы новичок в python, процесс совершенно несложный. Инструкция рассчитана на пользователей Windows. Если у вас возникли проблемы, пишите нам в чат ВК\(нижний правый угол\).

* **Установка зависимостей**. Если вы пользователь Windows, то самым лучшим способом работы с Python является среда Jupyter Notebook. Её вы можете установить, скачав пакет [Anaconda для Python 3](https://www.continuum.io/downloads). Приложение Jupyter Notebook появится среди установленных приложений, вы сможете найти его в поиске приложений Windows. Если вы используете другую операционную систему, вы можете так же установить _Anaconda_ или использовать менеджер пакетов _pip_

* **Скачиваем модуль.** Мы рекомендуем создать специальную папку на вашем компьютере, например по адресу _C:\Users\USER\_NAME\Documents\bcsquants. _ Скачайте файл [**bcsquants.py**](https://bcsquants.com/filer/sharing/1487762568/16/) и поместите его в в папку _ Documents\bcsquants_

* **Скачиваем данные**. Скачайте [данные \(55 MB\)](https://yadi.sk/d/BvChmsnI3ENbWe) и распакуйте архив в папку _Documents\bcsquants_, так чтобы папка _data_ была на том же уровне что и _bcsquants.py_

* **Осваиваем Jupyter Notebook**

Запустите приложение Jupyter Notebook из меню пуск или командой **jupyter notebook** в консоли. По умолчанию вы увидите в брузере вашу домашнюю директорию, перейдите в папку _Documents\bcsquants_. Создайте там рабочий .ipynb файл командой New-&gt;Python \[conda root\] или New-&gt;Python 3. Вставьте в первую ячейку рабочего файла следующий код:

```py
def init(self):
    self._tickSize = 'm5'
    self._window = 2
    self.alpha = 0.01 # 1 процент
    self.holdPeriod = 12 # 1 час 

def tick(self, data):
    predPrice = data['close'][0] # предпоследняя цена
    currentPrice = data['close'][1] # последняя цена

    if currentPrice >= (1 + self.alpha) * predPrice:
        order('buy', takeProfit=self.alpha, stopLoss=self.alpha, holdPeriod=self.holdPeriod)

    if currentPrice <= (1 - self.alpha) * predPrice:
        order('sell', takeProfit=self.alpha, stopLoss=self.alpha, holdPeriod=self.holdPeriod)

###############################################################################################
# Вставьте стратегию выше, смотри примеры https://bcsquants.com/doc/example

import bcsquants
from bcsquants import order

# Наведите курсор на середину названия функции getBacktestResult и нажмите shift+Tab, чтобы увидеть сигнатуру
result = bcsquants.getBacktestResult(init, tick)
bcsquants.showBacktestResult(result)
```

Нажмите на кнопку Play или Ctrl+Enter. Вы увидите таблицу с результатами бэктеста

Нажмите Insert -&gt; Insert Cell Below и вставьте следующий код

```py
% pylab
bcsquants.plotChart(result, 'BRX')
```

Запустите ячейку. Должно открыться отдельное окно с графиком. Масштабирование в нем осуществляется нажатием на крест сверху слева и правой кнопкой мыши.

* **Пишем стратегии**. Изучайте результаты ваших стратегий, просматривайте графики и участвуйте в соревновании. Не забудьте, что основной приз получат участники, которые покажут лучшие результаты на Online данных, а это значит чем раньше вы отправите ваши стратегии, тем больше у вас будет доступных данных. Более подробно о правилах смотрите в документации

### Данные

Тестовые данные представлены в формате .npy, [данные \(55 MB\)](https://yadi.sk/d/BvChmsnI3ENbWe)

Вы можете работать с ними из python3

```py
import numpy as np
np.load('close.npy')
# array([ 76.3 ,  76.15,  76.26, ...,  91.9 ,  91.82,  91.89])
np.load('time.npy', encoding='bytes')
# array([datetime.datetime(2016, 9, 1, 10, 0), ...], dtype=object)
```

Если вы хотите изучить данные не из python, можете скачать те же самые данные в формате CSV, [данные CSV \(54 MB\)](https://yadi.sk/d/FEKhXKt43ENbQC)

### Ограничения

Время выполнения стратегии ограничено 5 минутами на одном инструменте, суммарно не более 5 \* 16 = 80 минут на всё выполнение. Не рекомендуется писать стратегии, достигающие лимиты, иначе лимиты будут понижены

Память ограничена мягким лимитом в 3 GB

### Зависимости

Данные библиотеки доступны в бэктесте и могут пригодиться при локальной разработке, если вы ставили Anaconda, то все они установятся автоматически

* python 3.6
* numpy 1.11.2
* scipy 0.18.1
* scikit-learn 0.18.1
* matplotlib 1.5.3
* pandas 0.19.1
* statsmodels 0.8.0
* sympy 1.0
* и их зависимости

Дополнительно в нашем бэктесте доступны библиотеки:

* talib 0.4.0 - [https://github.com/mrjbq7/ta-lib](https://github.com/mrjbq7/ta-lib)
* tensorflow 1.0.1
* Theano 0.8.2
* xgboost 0.6
* keras 2.0.3 \(default backend tensorflow\)
* fbprophet 0.1.1
* pystan 2.14.0

Вам нужно будет ставить их самостоятельно

Если вам не хватает библиотек, просто напишите нам в чат \(в нижнем правом углу экрана\) или на почту **support@bcsquants.com**

