# Знакомство с Python

В первую очередь вам надо знать, что для участия в соревновании нет необходимости досконально владеть возможностями языка программирования python, но нужно знать основы.

Данный раздел во многом отвечает на вопрос, как изучить python с нуля за ограниченное количество времени.

Для работы с платформой соревнования вам не нужен предустановленный python на вашем компьютере, достаточно ввести код стратегии в окне отправки стратегии и отправить ее на тестирование, чтобы увидеть результат.

Однако для отладки и написания сложных стратегий все-таки полезно иметь возможность проводить расчеты и тестирование [локально](https://bcsquants.com/doc/localtest/) на вашем компьютере.

Для работы с python мы рекомендуем среду Jupyter Notebook, она позволяет пошагово выполнять расчеты, тестировать, хранить и выводить результаты. Для работы над стратегиями в Jupyter Notebook желательно убедиться, что у вас установлена библиотека numpy. Рекомендуем скачать Anaconda Python \([https://www.continuum.io/downloads](https://www.continuum.io/downloads)\), так как этот дистрибутив содержит все необходимые библиотеки и поддержку среды Jupyter Notebook.

Для участников, которые хотят быстро овладеть возможностями языка python и приступить к написанию торговых стратегий, можно рекомендовать экспресс-курс от Rick Muller: A Crash Course in Python for Scientists \([http://nbviewer.jupyter.org/gist/rpmuller/5920182](http://nbviewer.jupyter.org/gist/rpmuller/5920182)\), язык курса - английский. Также можно рекомендовать онлайн-книгу Dive Into Python 3 \([http://www.diveintopython3.net](http://www.diveintopython3.net)\). Кроме этого для быстрого старта можно посмотреть [Python Numpy Tutorial](http://cs231n.github.io/python-numpy-tutorial/)

Наиболее полно возможности языка python освещены в официальной документации \([https://www.python.org/doc/](https://www.python.org/doc/)\) также на английском языке.

Также для полного изучения языка подходит курс от Code Academy: [https://www.codecademy.com/ru/learn/python](https://www.codecademy.com/ru/learn/python)

Для изучения Python на русском языке можно посмотреть следующие ресурсы:

1. Вики-учебник по Python 3.1: [https://ru.wikibooks.org/wiki/Python/Учебник\_Python\_3.1](https://ru.wikibooks.org/wiki/Python/Учебник_Python_3.1)
2. Ресурс для изучения Python с нуля: [https://pythonworld.ru](https://pythonworld.ru)
3. Интерактивный учебник Python: [http://pythontutor.ru](http://pythontutor.ru)
4. Онлайн курс для начинающих изучение Python на русском языке: [https://stepik.org/course/Программирование-на-Python-67](https://stepik.org/course/Программирование-на-Python-67)



