#!/bin/bash

cd /home/deamoon/quantforces/bcsquants_doc 

git pull 2>&1

find . -iname '*.md' -exec sh -c 'markdown-tools render {} > {}.html' \; 2>&1

cp -R * /home/deamoon/quantforces/algosite/templates/doc
