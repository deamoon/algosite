# BCS Quant Challenge 2016
 Добро пожаловать на документацию по соревнованию [BCS Quant Challenge 2016](http://QuantForces.com/bcs) на платформе [QuantForces](http://QuantForces.com)
 
# О соревновании

Cоревнование по алгоритмической торговле компании БКС – это соревнование по разработке торговых роботов, которые, используя актуальные биржевые данные (цены акций и фьючерсов), совершают сделки по покупке и продаже на заданном наборе инструментов. Перед участниками стоит задача написания торговой стратегии, обеспечивающей максимальное качество.  

Участникам соревнования доступен интерфейсс написания торговых стратегий на языке python (версии 3).

Торговая стратегия – это алгоритм совершения сделок на бирже (покупка или продажа фиксированного лота на различных инструментах биржи).

Каждая написанная стратегия может быть отправлена на тестирование на реальных данных биржи. Результатом тестирования является торговля стратегии на тестовых данных (они открыты и их можно изучить), на контрольных данных (они используются для предварительной оценки качества стратегии), а также на онлайн данных (учитываются только данные биржи, поступившие после запуска стратегии в онлайн режим). Каждый игрок получает очки соревнования за положительный результат на онлайн данных.

# Что добавить
1. Не очень понятно про разделение данных на 3 вида
2. Написать про обратную связь и группу
3. Инструкция как установить питон и где почитать про него
4. Локальное тестирование стратегии
5. Нужен туториал, краткая инструкция, для тех кому хочется сначала попробовать, а потом читать документацию
6. Нужен пример, как дорабатывать стратегию от неприбыльной к прибыльной, как анализировать и на что смотреть
7. Проверяем документацию