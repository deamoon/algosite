# Шаблоны торговых стратегий

Ниже представлены шаблоны самых простых торговых идей, которые помогут вам изучить, как устроена наша платформа. Просто скопируйте представленные ниже примеры в [окно отправки стратегии](https://bcsquants.com/send/) и посмотрите, как торгует та или иная стратегия.

Детально параметры стратегий и исходный код рассматриваются в разделе документации [Стратегия](https://bcsquants.com/doc/attempt/).

### Стратегия Купи и Держи

```python
def init(self):
    self._tickSize = 'm5'
    self.alpha = 1000
    self.holdPeriod = 1000*1000*1000*1000
    self.doneOrder = False

def tick(self, data):
    if not self.doneOrder:
        order('buy', takeProfit=self.alpha, stopLoss=self.alpha/2, holdPeriod=self.holdPeriod)
        self.doneOrder = True
```

Классика инвестирования, результатом данной стратегии является рост цены актива за рассматриваемый период. Хорошо использовать данный результат как ориентир при написании собственных стратегий, лучше ли ваш алгоритм простой покупки инструмента?

Кроме всего прочего, вы можете попытаться сразу улучшить качество данного алгоритма, увеличив количество сделок в этой стратегии. Для этого достаточно ограничить параметр takeProfit и убрать ограничение на повторную отправку ордера \(удалить строчку self.doneOrder = True\).

### Стратегия Момент

```python
import numpy as np

def getReturn(array):
    return (array[1:] - array[:-1]) / array[:-1]

def init(self):
    self.size = 100
    self.alpha = 0.02
    self._window = self.size
    self._tickSize = 'm5'

def tick(self, data):
    price = data['close'][-1]
    window = data['close']

    if np.sum(getReturn(window)) > self.alpha and price > np.average(window):
        order('buy', takeProfit=self.alpha, stopLoss=self.alpha/2, holdPeriod=self.size*3)
```

В данной стратегии используется функции getReturn, вычисляющая изменение цены в текущем окне. Если цена в настоящий момент растет, стратегия заходит в позицию.

### Стратегия Тренд

```python
def init(self):
    self._tickSize = 'm5'
    self._window = 6 * 12 # 6 hours    
    self.alpha = 0.01
    self.order = {
        'takeProfit': 4 * self.alpha, # 4%
        'stopLoss': 2 * self.alpha, # 2%
        'holdPeriod': 1 * self._window, # 12 hours
    }

def tick(self, data):
    window = data['close']
    priceStart = window[0] # the first close price
    priceEnd = window[-1] # the last close price
    returnPrice = (priceEnd - priceStart) / priceStart

    if returnPrice > self.alpha:
        order('buy', **self.order) # the same as
      # order('buy', takeProfit=self.order['takeProfit'], 
      #              stopLoss=self.order['stopLoss'], 
      #              holdPeriod=self.order['holdPeriod'])

    if returnPrice < -self.alpha:
        order('sell', **self.order)
```

Стратегия заходит в позицию в соответствии с направлением тренда движения цены.

### Стратегия Шарп

```python
import numpy as np 

def init(self): 
    self._tickSize = 'm5' 
    self._window = 3 * 9 * 12 # примерно 3 торговых дня 
    self.alpha = 0.002 
    self.order = { 
        'takeProfit': 0.02, 
        'stopLoss': 0.01, 
        'holdPeriod': self._window // 3, # примерно 1 торговый день 
    } 

def getSharp(window): 
    priceReturn = (window[1:] - window[:-1]) / window[1:] 
    std = np.std(priceReturn) 
    return np.average(priceReturn) / std if std else 0 

def tick(self, data): 
    window = data['close'] 
    sharp = getSharp(window) 

    if sharp < -self.alpha: 
        order('sell', **self.order) 

    if sharp > self.alpha: 
        order('buy', **self.order)
```

Стратегия использует коэффициент Шарпа для определения момента входа в позицию. Данный коэффициент — один из базовых показателей эффективности инвестиционного актива, который вычисляется как отношение средней доходности к среднему отклонению стоимости актива.

### Стратегия Возврат к среднему

```python
import numpy as np 

def init(self): 
    self._tickSize = 'm5' 
    self._window = 2 * 5 * 9 * 12 # 2 недели 
    self.alpha = 0.05 
    self.order = { 
    'takeProfit': 0.02, 
    'stopLoss': 0.01, 
    'holdPeriod': 3 * 9 * 12, # 3 дня 
} 

def getMinMax(window): 
    return np.min(window), np.max(window) 

def tick(self, data): 
    window = data['close'] 
    minPrice, maxPrice = getMinMax(window) 
    gap = (maxPrice-minPrice)/minPrice 
    std = 0.6*gap 
    price = window[-1] 

    if (price - minPrice) / minPrice >= std: 
        order('sell', **self.order) 
    elif (maxPrice - price) / price >= std: 
        order('buy', **self.order)
```

Если цена значительно отклонилась от среднего вниз, стратегия посылает ордер на покупку инструмента, и наоборот, при значительном отклонении цены вверх, осуществляется продажа.

