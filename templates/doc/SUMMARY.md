# Summary

* [Добро пожаловать](README.md)
* [Туториал](tutorial.md)
* [Стратегия](attempt.md)
* [Данные](data.md)
* [Шаблоны торговых стратегий](example.md)
* [Рейтинги и победители](rating.md)
* [Знакомство с Python](python.md)
* [Локальное тестирование](localtest.md)
* [Сотрудничество](partner.md)
* [Ответы на вопросы](faq.md)

