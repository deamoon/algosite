# Туториал

Торговая стратегия – это алгоритм совершения сделок на бирже \(покупка или продажа фиксированного лота на различных инструментах биржи\). Для написания стратегии вам доступен язык программирования python и популярные библиотеки, подробнее смотрите в разделе [Локальное тестирование](https://bcsquants.com/doc/localtest/) -&gt; Зависимости.

Для отправки стратегии, зайдите в [Создать стратегию](https://bcsquants.com/send/).

![](https://bcsquants.com/filer/sharing/1484666345/3/)

Коротко разберем, что из себя представляет стратегия. Для более подробного описания читайте раздел [Стратегия](https://bcsquants.com/doc/attempt/).

Вы видите две python-функции **init\(self\)** и **tick\(self, data\)**.

В функции **init\(self\)** вы создаете новые поля и сохраняете их в переменную **self.** Такие поля как **alpha** и **holdPeriod** являются пользовательскими. Поля, начинающиеся с нижнего подчеркивания являются параметрами бэктеста. На данный момент доступны 2 параметра:

**\_tickSize** - размер **тика**, может принимать одно из значений **\['m5', 'm1'\]**. Где 'm5' означает 5 минут, а 'm1' - 1 минута. Это поле определяет сколько данных получит ваша стратегия, соответственно если вы выберете m1, то получите в 5 раз больше данных, чем если выберете m5.

**\_window** - длина окна **в тиках. **Понятие окна напрямую связано с переменной **data.**

В функции **tick\(self, data\) **происходит основная работа. Эта функция вызывается в основном цикле бэктеста при итерации по историческим данным. **self** в функции **tick**, тот же самый что и в функции **init**. Исторические данные доступны в переменной **data **- словарь с ключами** \['time', 'open', 'high', 'low', 'close', 'volume', 'count'\] **и значениями **numpy array. **Для тех, кто пока не знаком с библиотекой **numpy**, можно считать, что это просто стандартный **python list** со сверхспособностями \(например, вычисление среднего по списку функцией **mean**\), посмотреть какие возможности предоставляет numpy array можно на [странице документации](https://docs.scipy.org/doc/numpy-dev/user/quickstart.html#universal-functions). Словарь содержит следующие ключи:

* time - numpy array datetime, время начала тика;
* open - numpy array float, цена открытия тика;
* high - numpy array float, максимальная цена тика;
* low - numpy array float, минимальная цена тика;
* close - numpy array float, цена закрытия тика;
* volume - numpy array int, объём тика;
* count - numpy array int, количество сделок тика.

Подробнее о тиках смотрите в разделе [Данные](https://bcsquants.com/doc/data/) -&gt; Тики.

Чтобы обратиться к списку цен закрытия используйте **data\['close'\]**. Длина всех списков в **data** всегда одинаковая.  Если вы определили длину окна **self.\_window,** то длина списков всегда будет равна длине окна \(то есть тестирование начнется не с первого тика, а с размера окна\). Если вы присвоите **None** в длину окна или совсем удалите эту настройку, то длина списков будет расширяться от единицы до длины истории, то есть в любой момент времени вам будет доступна вся известная на данный момент история, начиная с одного тика.

В каждый момент вызова функции **tick**, вы можете отправить заявку, используя функцию **order\(direct, takeProfit, stopLoss, holdPeriod\). **Параметры функции** order:**

* direct - python str, принимает значение из \['buy', 'sell'\], определяет покупает робот или продаёт;
* takeProfit - python float, параметр выхода из позиции по достижению определенного уровня прибыли, выражается в долях, а не процентах, то есть для задания уровня прибыли 2%, надо определить takeProfit = 0.02;
* stopLoss - python float, нижняя граница заявки, определяется в долях, а не в процентах, то есть 1% = 0.01;
* holdPeriod - python int, размер в **тиках **продолжительности заявки.

Подробнее о формате заявок смотрите в [Стратегия](https://bcsquants.com/doc/attempt/) -&gt; Заявки.

Немного поняв, что происходит, перейдём дальше. Нажмите отправить стратегию и вы попадете на экран финансового результата стратегии.

![](https://bcsquants.com/filer/sharing/1484666581/4/)

Это экран, на котором вы будете проводить большую часть времени, здесь вы можете анализировать поведение вашей торговой стратегии на истории. Давайте по порядку познакомимся с основными понятиями в таблице и на графике.

Прежде всего напомним, что исторические данные разделены на тестовые и контрольные дни, об этом написано на [главной странице](https://bcsquants.com/doc/) документации. Перечислим все столбцы таблицы результатов:

* Инструмент - название финансового инструмента;
* Контрольное качество - качество стратегии на контрольных данных, функция от прибыли, количества сделок, доли прибыльных сделок и т.п.

**Все поля ниже считаются только по тестовым данным:**

* Инструмент - название финансового инструмента, на котором получен данный результат, всего представлено 16 инструментов Московской биржи;
* P&L - прибыль или убыток \(если значение меньше 0\) стратегии на всей тестовой выборке, в процентах;
* Тестовый результат - это виртуальное количество очков рейтинга, которые бы получила стратегия, если бы была запущена в онлайн не тестовой выборке;

* Стандартное отклонение - мера устойчивости величины прибыли во времени на протяжении тестирования;

* Количество сделок - количество совершенных сделок, открытие и закрытие позиции учитываются как одна сделка;

* Выход по TP - прибыль по сделкам, закрывшимся по параметру takeProfit;

* Выход по HP - прибыль или убыток по сделкам, закрывшимся по параметру holdingPeriod;

* Выход по SL - убыток по сделкам, закрывшимся по параметру stopLoss;

* Тестовое качество - общая мера качества стратегии \(является функцией от соотношения прибыльных и убыточных сделок, суммарного P&L, средней прибыли и среднего убытка по сделкам и количества сделок\).

* Заявки - CSV файл с заявками;

* Сделки - CSV файл со сделками.

Одна ваша стратегия запускается независимо на каждом из 16 инструментов.

Задача бэктеста запустить вашу стратегию, получить от неё заявки \(вызовы функции order\) и перевести их в сделки. Правило перевода из заявок в сделки следующее:

**Если на данный момент сделка не открыта и приходит заявка, зайти на рынок по этой заявке по цене закрытия следующего тика. Если сделка открыта и приходит заявка, проигнорировать заявку. Закрыть сделку по цене закрытия следующего тика, когда выполнено одно из условий заявки, по которой открыта сделка, то есть takeProfit, stopLoss или holdPeriod и вычесть комиссию из прибыли по сделке.**

Удобнее всего анализировать сделки на графике. Кликнув на название инструмента, открывается график истории и P&L. Моменты входа в сделку отмечаются **желтым** кружком с соответствующей буквой **B - buy** или **S - sell**. Прибыльные сделки отмечаются зеленым кружком\(сделки вышедшие по takeProfit и прибыльные по holdPeriod\), убыточные красным \(сделки вышедшие по stopLoss и убыточные по holdPeriod\). Приняты следующие буквенные обозначения: **P - takeProfit**, **L - stopLoss**, **H - holdPeriod.**

# Что дальше?

1. Попробуйте изменить параметры стратегии, размер окна, величины takeProfit и посмотреть, как это отражается на результатах. **Помните, что мы не рекомендуем отправлять вычислительно сложные стратегии без предварительного тестирования на **[**локальном бэктесте**](https://bcsquants.com/doc/localtest/)**. **
2. В примере используется стандартное скользящее среднее, это самый известный технический индикатор. Вы можете найти описание многих других индикаторов в [нашей группе](http://vk.com/bcsquants) и в Интернете. Предварительно посмотреть некоторые индикаторы можно на графике, нажав на кнопку Индикаторы ![](https://bcsquants.com/filer/sharing/1484827830/5/)
3. Воплощайте разные идеи, читайте документацию. Не забудьте, что у вас только **10 посылок в онлайн** и вам нужно грамотно ими распорядиться за период соревнования. Мы всегда готовы помочь вам, просто пишите нам на почту **support@bcsquants.com** или в чат.



