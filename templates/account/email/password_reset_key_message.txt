{% load i18n %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Здравствуйте

Вы получили это письмо, потому что вы или кто-то другой запросили восстановление пароля для вашей почты. Перейдите по ссылке ниже, чтобы восстановить пароль{% endblocktrans %}

{{ password_reset_url }}

{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}С уважением команда BcsQuants
{{ site_domain }}{% endblocktrans %}
