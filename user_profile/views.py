from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def profile(request):
    context = {
        'name_user': "dima",
    }
    return render(request, 'user_profile/profile.html', context)
