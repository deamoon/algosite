from django.contrib import admin
from models import Article, ExampleStrategy

class ArticleAdmin(admin.ModelAdmin):
    change_form_template = 'blog/admin/change_form.html'

admin.site.register(Article, ArticleAdmin)
admin.site.register(ExampleStrategy)