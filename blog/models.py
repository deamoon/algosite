#!/usr/bin/python
#coding=utf-8

from django.db import models

class ExampleStrategy(models.Model):
    LANGUAGE_LIST = (
        ('python2', 'Python 2.7'),
        ('cpp', 'G++ 4.6.3')
    )
    language = models.CharField(max_length=20, choices=LANGUAGE_LIST) # язык кода посылки
    code = models.TextField() # текст кода посылки

    def __unicode__(self):
        return '#{0} {1}'.format(self.id, self.language)

class Article(models.Model):
    """
    Абстрактная модель задачи, используется для хранения важной технической информации,
    которая необходима для тестирующей системы
    """
    name = models.CharField(max_length=200) # имя задачи
    description = models.TextField() # описание задачи
    examples = models.ManyToManyField(ExampleStrategy)

    def __unicode__(self):
        return self.name