from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Article

def detail(request, article_id):
    context = {
        'article': get_object_or_404(Article, pk=article_id)
    }
    return render(request, 'blog/detail.html', context)

def index(request):
    context = {
        'articles': Article.objects.all()
    }
    return render(request, 'blog/index.html', context)