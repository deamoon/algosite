# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='ExampleStrategy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(max_length=20, choices=[(b'python2', b'Python 2.7'), (b'cpp', b'G++ 4.6.3')])),
                ('code', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='examples',
            field=models.ManyToManyField(to='blog.ExampleStrategy'),
        ),
    ]
