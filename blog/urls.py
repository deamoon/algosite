from django.conf.urls import patterns, url
import views

urlpatterns = patterns(
    '',
    url(r'^(?P<article_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^$', views.index, name='index'),
    # url(r'^(?P<attempt_id>[0-9]+)/data$', views.data, name='data'),
)