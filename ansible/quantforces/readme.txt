Настройка сервера vscale.io:
    $ adduser deamoon
    $ visudo
    deamoon    ALL=(ALL:ALL) ALL
    $ cd /home/deamoon
    $ mkdir .ssh
    $ chmod 700 .ssh
    $ nano .ssh/authorized_keys
    add public key
    $ sudo chown -R deamoon:deamoon .ssh
    $ sudo apt-get update
    $ sudo apt-get dist-upgrade

Install ansible:
    $ sudo apt-get install -y libffi-dev libssl-dev libyaml-cpp-dev libyaml-dev
    $ sudo pip install ansible
    $ sudo ansible-galaxy install DavidWittman.redis

Install redis:
    Change hosts in playbook.yml redis on all
    hosts: all
    $ ansible-playbook -i "localhost," -c local -t redis --ask-sudo-pass playbook.yml
    return hosts in playbook.yml

What add:
    dump.json download and loaddata(instruction in deploy.txt)
    change hostname
    add to /etc/hosts:
        40.118.71.134 backtest1
        40.68.165.170 backtest5
    add worker.key for backtest with permissions 600
    add .bashrc:
        eval `ssh-agent -s`
        ssh-add $HOME/worker.key
    check connection to backtests without password
    check CALLBACK_URL in backtest if new adress not quantforces.com
    check date (sudo dpkg-reconfigure tzdata; set Europe, Kiev)
    restart cron service after change time zone data
    add Documentation
    update kernel
    check date on server

Sync data:
    copy databox folder:
        rsync -azP -e "ssh -p 1907" databox deamoon@92.242.44.43:/home/deamoon/quantforces/static_content

Backtest:
    add proxy to nginx
    celery -A backtest flower --address=192.168.100.101
    go to bcsquant.com:2002

Documentation:
    sudo apt-get install nodejs npm
    sudo npm install -g markdown-tools
    git clone https://git.gitbook.com/deamoon/bcsquants_doc.git
    npm install
    find . -iname '*.md' -exec sh -c 'markdown-tools render {} > {}.html' \;
    git config credential.helper 'store --file /home/deamoon/.my-credentials'
    copy .my-credentials from files folder
    git pull

/home/deamoon/.my-credentials:
    https://deamoon:diman95git@git.gitbook.com