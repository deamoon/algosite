---

- name: SSH tightening
  hosts: all
  become: yes
  become_method: sudo
  ignore_errors: yes
  tasks:
    - name: Disable root's ssh account
      action: >
        lineinfile
        dest=/etc/ssh/sshd_config
        regexp="^PermitRootLogin"
        line="PermitRootLogin no"
        state=present
      notify: Restart ssh
    - name: Disable password authentication
      action: >
        lineinfile
        dest=/etc/ssh/sshd_config
        regexp="^PasswordAuthentication"
        line="PasswordAuthentication no"
        state=present
      notify: Restart ssh

  handlers:
  - name: Restart ssh
    action: service name=ssh state=restarted

- name: Update APT package cache and locale
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Update APT package cache
      action: apt update_cache=yes

    - name: add locals
      lineinfile: >
        dest=/etc/default/locale
        line='LC_ALL="en_US.UTF-8"'

- name: Set timezone to UTC
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Set timezone variables
      copy: >
        content='Europe/Moscow'
        dest=/etc/timezone
        owner=root
        group=root
        mode=0644
        backup=yes
      notify:
        - Update timezone
  handlers:
    - name: Update timezone
      command: >
        dpkg-reconfigure
        --frontend noninteractive
        tzdata

- name: Syncronise clocks
  hosts: all
  become: yes
  become_method: sudo
  tasks:
    - name: install ntp
      apt: name=ntp

    - name: copy ntp config
      copy: src=files/ntp.conf dest=/etc/ntp.conf

    - name: restart ntp
      service: name=ntp state=restarted

- name: Setup unattended upgrades
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Install unattended upgrades package
      apt: name=unattended-upgrades
      notify:
        - dpkg reconfigure

  handlers:
    - name: dpkg reconfigure
      command: >
        dpkg-reconfigure
        --frontend noninteractive
        -plow unattended-upgrades

- name: Setup Postresql Database
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  vars_files:
    - var.yml
  tasks:
    - apt_repository: >
        repo='deb https://apt.postgresql.org/pub/repos/apt/ precise-pgdg main'
        state=present

    - shell: wget --quiet -O - https://postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    - apt: name=postgresql-9.4 update_cache=yes

    - name: Install PostgreSQL Libs
      apt: name={{ item }} state=installed
      with_items:
        - postgresql-contrib
        - python-psycopg2
        - libpq-dev

    - name: Ensure database is created
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_db: name={{ db_name }}
                     encoding='UTF-8'
                     lc_collate='en_US.UTF-8'
                     lc_ctype='en_US.UTF-8'
                     template='template0'
                     state=present

    - name: Ensure user has access to the database
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_user: db={{ db_name }}
                       name={{ db_user }}
                       password={{ db_password }}
                       priv=ALL
                       state=present

    - name: Ensure user does not have unnecessary privileges
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_user: name={{ db_user }}
                       role_attr_flags=NOSUPERUSER,NOCREATEDB
                       state=present

    # - name: Add remote host
    #   lineinfile: >
    #     dest=/etc/postgresql/9.4/main/postgresql.conf
    #     line="listen_addresses = '{{ db_host }}'"

    # - name: Add remote mask
    #   lineinfile: >
    #     dest=/etc/postgresql/9.4/main/pg_hba.conf
    #     line='host    all             all             {{ db_mask }}         trust'

    - name: restart postgresql
      service: name=postgresql state=restarted enabled=yes

- name: Configure the master redis server
  hosts: all
  vars_files:
    - var.yml
  become: yes
  become_method: sudo
  tags: redis
  roles:
    - DavidWittman.redis

- name: Setup App Server(s)
  hosts: all
  become: yes
  become_method: sudo
  vars_files:
    - var.yml
  tasks:
    - name: Install python virtualenv
      apt: name=python-virtualenv

    - name: Install python dev
      apt: name=python-dev

    - name: Install git
      apt: name=git

    - name: Install Pillow dependencies
      apt: name={{ item }} state=installed
      with_items:
        - libjpeg-dev
        - libfreetype6
        - libfreetype6-dev
        - zlib1g-dev
        - libjpeg8-dev
        - libpq-dev

    - name: Install zip
      apt: name={{ item }} state=installed
      with_items:
        - zip
        - unzip

    - copy: >
        src=files/algosite_git.key dest={{ home_folder }}
        mode=0600
      become_user: "{{ my_user }}"

    - file: >
        path={{ home_folder }}/.ssh/known_hosts
        owner={{ my_user }}
        group={{ my_user }}
        mode=755
        state=touch

    - name: Creates directory quantforces
      file: path={{ item }} state=directory
      become_user: "{{ my_user }}"
      with_items:
        - "{{ home_folder }}/quantforces"
        - "{{ home_folder }}/quantforces/logs"
        - "{{ home_folder }}/quantforces/pids"
        - "{{ home_folder }}/quantforces/static_content"
        - "{{ home_folder }}/quantforces/static_content/static"
        - "{{ home_folder }}/quantforces/static_content/media"
        - "{{ home_folder }}/quantforces/static_content/databox"
        - "{{ home_folder }}/quantforces/static_content/quantbox"


    - name: Checkout Django code
      git: >
        repo=git@bitbucket.org:deamoon/algosite.git
        dest={{ home_folder }}/quantforces/algosite
        accept_hostkey=yes
        key_file={{ home_folder }}/algosite_git.key
        force=yes
      become_user: "{{ my_user }}"

    - name: Change Debug Mode
      lineinfile: >
        dest={{ home_folder }}/quantforces/algosite/algosite/settings.py
        line="DEBUG = True"

    - file: >
        path={{ home_folder }}/quantforces
        owner={{ my_user }}
        group={{ my_user }}
        mode=755
        state=directory
        recurse=yes

    - name: Install Python requirements
      pip: >
        requirements={{ home_folder }}/quantforces/algosite/requirements.txt
        virtualenv={{ home_folder }}/.virtualenvs/{{ venv }}
      become_user: "{{ my_user }}"

    - file: >
        path={{ home_folder }}/.virtualenvs/{{ venv }}
        owner={{ my_user }}
        group={{ my_user }}
        mode=755
        state=directory
        recurse=yes

    - template: >
        src=files/env.sh
        dest={{ home_folder }}/.virtualenvs/{{ venv }}/exec
        mode=755
      become_user: "{{ my_user }}"

    - template: >
        src=files/env.sh
        dest={{ home_folder }}/env.sh
        mode=755
      become_user: "{{ my_user }}"

    - command: >
        {{ home_folder }}/.virtualenvs/{{ venv }}/exec
        python manage.py migrate --noinput
      args:
        chdir: '{{ home_folder }}/quantforces/algosite'
      become_user: "{{ my_user }}"

    - command: >
        {{ home_folder }}/.virtualenvs/{{ venv }}/exec
        python manage.py collectstatic --noinput
      args:
        chdir: '{{ home_folder }}/quantforces/algosite'
      become_user: "{{ my_user }}"

    - name: Install supervisor
      apt: name=supervisor

    - template: >
        src=files/supervisord.conf
        dest=/etc/supervisor/conf.d/django_app.conf

    - supervisorctl: name=algosite state=stopped
      ignore_errors: yes

    - command: killall gunicorn
      ignore_errors: yes

    - command: /usr/bin/supervisorctl reload

    - file: >
        path={{ home_folder }}/quantforces/pids/gunicorn.pid
        state=absent

    - command: supervisorctl reread

    - supervisorctl: name=algosite state=started
      ignore_errors: yes

    - supervisorctl: name=celery state=restarted
      ignore_errors: yes

    - name: Install nginx
      apt: name=nginx

    - name: copy nginx config file
      template: >
        src=files/nginx-app.conf
        dest=/etc/nginx/sites-available/default
    - name: enable configuration
      file: >
        dest=/etc/nginx/sites-enabled/default
        src=/etc/nginx/sites-available/default
        state=link
    - service: name=nginx state=restarted

# - name: Setup Load balancer(s)
#   hosts: load_balancers
#   become: yes
#   become_method: sudo
#   vars_files:
#     - var.yml
#   tasks:
#     - apt: name=nginx

#     - name: copy nginx config file
#       template: >
#         src=files/nginx-load-balancer.conf
#         dest=/etc/nginx/sites-available/default
#     - copy: src=files/nginx.key dest=/etc/nginx/ssl/
#     - copy: src=files/nginx.crt dest=/etc/nginx/ssl/

#     - name: enable configuration
#       file: >
#         dest=/etc/nginx/sites-enabled/default
#         src=/etc/nginx/sites-available/default
#         state=link
#     - service: name=nginx state=restarted
