#!/bin/bash
source {{ home_folder }}/.virtualenvs/{{ venv }}/bin/activate

HOME={{ home_folder }}

export BD_USER={{ db_user }}
export BD_PASSWORD={{ db_password }}
export BD_ALGOSITE_DATABASE_NAME={{ db_name }}
export BD_HOST={{ db_host }}
export BD_PORT={{ db_port }}

export ALGOSITE_STATIC=$HOME/quantforces/static_content/static
export ALGOSITE_MEDIA=$HOME/quantforces/static_content/media

export ALGOSITE_DEBUG=0

export ALGOSITE_SANDBOX=$HOME/quantforces/static_content/quantbox
export ALGOSITE_DATABOX=$HOME/quantforces/static_content/databox

$@
