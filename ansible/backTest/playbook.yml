---

- name: SSH tightening
  hosts: all
  become: yes
  become_method: sudo
  tasks:
    - name: Disable root's ssh account
      action: >
        lineinfile
        dest=/etc/ssh/sshd_config
        regexp="^PermitRootLogin"
        line="PermitRootLogin no"
        state=present
      notify: Restart ssh
    - name: Disable password authentication
      action: >
        lineinfile
        dest=/etc/ssh/sshd_config
        regexp="^PasswordAuthentication"
        line="PasswordAuthentication no"
        state=present
      notify: Restart ssh

  handlers:
  - name: Restart ssh
    action: service name=ssh state=restarted

- name: Update APT package cache and locale
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Update APT package cache
      action: apt update_cache=yes

    - name: add locals
      lineinfile: >
        dest=/etc/default/locale
        line='LC_ALL="en_US.UTF-8"'

- name: Set timezone to UTC
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Set timezone variables
      copy: >
        content='Europe/Moscow'
        dest=/etc/timezone
        owner=root
        group=root
        mode=0644
        backup=yes
      notify:
        - Update timezone
  handlers:
    - name: Update timezone
      command: >
        dpkg-reconfigure
        --frontend noninteractive
        tzdata

- name: Syncronise clocks
  hosts: all
  become: yes
  become_method: sudo
  tasks:
    - name: install ntp
      apt: name=ntp

    - name: copy ntp config
      copy: src=files/ntp.conf dest=/etc/ntp.conf

    - name: restart ntp
      service: name=ntp state=restarted

- name: Setup unattended upgrades
  hosts: all
  gather_facts: False
  become: yes
  become_method: sudo
  tasks:
    - name: Install unattended upgrades package
      apt: name=unattended-upgrades
      notify:
        - dpkg reconfigure

  handlers:
    - name: dpkg reconfigure
      command: >
        dpkg-reconfigure
        --frontend noninteractive
        -plow unattended-upgrades

- name: Setup Docker
  hosts: workers
  gather_facts: False
  become: yes
  become_method: sudo
  vars_files:
    - var.yml
  tasks:
    - name: Install Docker Kernal Libs
      apt: name={{ item }} state=installed update_cache=yes
      with_items:
        # - linux-image-extra-$(uname -r)
        - linux-image-extra-virtual
        - apt-transport-https
        - ca-certificates
        - curl
        - software-properties-common
        - python-pip

    - raw: sudo apt-get install linux-image-extra-$(uname -r)
    - raw: curl -fsSL https://apt.dockerproject.org/gpg | sudo apt-key add -
    - raw: sudo add-apt-repository "deb https://apt.dockerproject.org/repo/ ubuntu-$(lsb_release -cs) main"

    - name: Install Docker
      apt: name={{ item }} state=installed update_cache=yes
      with_items:
        - docker-engine

    - group:
        name: docker
        state: present

    - name: add deamoon to docker group
      user:
        name: "{{ my_user }}"
        groups: docker
        append: yes

    - pip:
        name: docker-py
        version: 1.7.0

    # - name: pull an image
    #   docker_image:
    #     name: deamoon/bcs_backtest:latest

- name: Setup Postresql Database
  hosts: workers
  gather_facts: False
  become: yes
  become_method: sudo
  vars_files:
    - var.yml
  tasks:
    - apt_repository: >
        repo='deb https://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main'
        state=present

    - shell: wget --quiet -O - https://postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    - apt: name=postgresql-9.4 update_cache=yes

    - name: Install PostgreSQL Libs
      apt: name={{ item }} state=installed
      with_items:
        - postgresql-contrib
        - python-psycopg2
        - libpq-dev

    - name: Ensure the PostgreSQL service is running
      service: name=postgresql state=started enabled=yes

    - name: Ensure database is created
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_db: name={{ db_name }}
                     encoding='UTF-8'
                     lc_collate='en_US.UTF-8'
                     lc_ctype='en_US.UTF-8'
                     template='template0'
                     state=present

    - name: Ensure user has access to the database
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_user: db={{ db_name }}
                       name={{ db_user }}
                       password={{ db_password }}
                       priv=ALL
                       state=present

    - name: Ensure user does not have unnecessary privileges
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true
      postgresql_user: name={{ db_user }}
                       role_attr_flags=NOSUPERUSER,NOCREATEDB
                       state=present

    # - name: Add remote host
    #   lineinfile: >
    #     dest=/etc/postgresql/9.3/main/postgresql.conf
    #     line="listen_addresses = '{{ db_host }}'"

    # - name: Add remote mask
    #   lineinfile: >
    #     dest=/etc/postgresql/9.3/main/pg_hba.conf
    #     line='host    all             all             {{ db_mask }}         trust'

    - name: restart postgresql
      service: name=postgresql state=restarted enabled=yes

- name: Setup App Server(s)
  hosts: workers
  become: yes
  become_method: sudo
  tags: setup_server
  vars_files:
    - var.yml
  tasks:
    - name: Add aliases
      lineinfile: >
        dest={{ home_folder }}/.bashrc
        line="{{ item }}"
      with_items:
        - "alias python=python3"
        - "alias pip=pip3"
      become_user: "{{ my_user }}"

    - name: Install Dependencies
      apt: name={{ item }} state=installed
      with_items:
        - "python-dev"
        - "git"
        - "python-pip"
        - "python3-pip"
        - "supervisor"
        - "wget"
        - "unzip"

    - pip: name=virtualenv executable=pip3

    - copy: >
        src=files/quant.key dest={{ home_folder }}
        mode=0600
      become_user: "{{ my_user }}"

    - name: Update bashrc
      lineinfile:
        dest: "{{ home_folder }}/.bashrc"
        create: True
        line: "{{ item.line }}"
      with_items:
        - { line: "eval `ssh-agent -s`" }
        - { line: "ssh add {{ home_folder }}/quant.key" }

    - copy: >
        src=files/restart.sh dest={{ home_folder }}
        mode=0700
      become_user: "{{ my_user }}"

    - name: Checkout backtest code
      git: >
        repo=git@bitbucket.org:deamoon/backtest.git
        dest={{ backtest_folder }}
        accept_hostkey=yes
        key_file={{ home_folder }}/quant.key
        force=yes
      become_user: "{{ my_user }}"

    # - name: Change Debug Mode
    #   lineinfile: >
    #     dest={{ home_folder }}/quantforces/algosite/settings.py
    #     line="DEBUG = True"

    - name: Creates directory
      file: path={{ item }} state=directory
      with_items:
        - "{{ log_folder }}"
        - "{{ pid_folder }}"
      become_user: "{{ my_user }}"

    - template: >
        src=files/venv_activate.sh
        dest={{ home_folder }}/env.sh
        mode=755
      become_user: "{{ my_user }}"

    - template: >
        src=files/perfomance.sh
        dest={{ home_folder }}/perfomance.sh
        mode=755
      become_user: "{{ my_user }}"

    - name: Install Python requirements
      pip: >
        requirements={{ item }}/requirements.txt
        virtualenv={{ home_folder }}/.virtualenvs/{{ venv }}
    #    executable=pip3
      with_items:
        - "{{ backtest_folder }}"
      become_user: "{{ my_user }}"

    - file: >
        path={{ item }}
        owner={{ my_user }}
        group={{ my_user }}
        mode=755
        state=directory
        recurse=yes
      with_items:
        - "{{ backtest_folder }}"
        - "{{ home_folder }}/.virtualenvs/{{ venv }}"

    - template: >
        src=files/venv_activate.sh
        dest={{ home_folder }}/.virtualenvs/{{ venv }}/exec
        mode=755
      become_user: "{{ my_user }}"

    - command: >
        {{ home_folder }}/.virtualenvs/{{ venv }}/exec
        python3 manage.py migrate --noinput
      args:
        chdir: "{{ backtest_folder }}"
      become_user: "{{ my_user }}"

    - template: >
        src=files/supervisord.conf
        dest=/etc/supervisor/conf.d/backtest.conf

    - supervisorctl: name=backtest state=stopped
      ignore_errors: yes

    - command: killall gunicorn
      ignore_errors: yes

    - command: /usr/bin/supervisorctl reload

    - file: >
        path={{ pid_folder }}/gunicorn.pid
        state=absent

    - supervisorctl: name=backtest state=started

    - supervisorctl: name=celery state=restarted

    - name: Install nginx
      apt: name=nginx

    - name: copy nginx config file
      template: >
        src=files/nginx-app.conf
        dest=/etc/nginx/sites-available/default

    - name: enable configuration
      file: >
        dest=/etc/nginx/sites-enabled/default
        src=/etc/nginx/sites-available/default
        state=link

    - service: name=nginx state=restarted

    # Install Redis
    # Download Data
    # wget -r -nH -np --cut-dirs=1 --no-check-certificate -U Mozilla --user=moy-biblio --password=biblio https://debdav.yandex.ru/BCS.zip
    # Do save user for running algo's

- name: Configure the master redis server
  hosts: workers
  vars_files:
    - var.yml
  become: yes
  become_method: sudo
  tags: redis
  roles:
    - DavidWittman.redis

- name: Download database
  hosts: workers
  vars_files:
    - var.yml
  tags: BCS_backtest
  become_user: "{{ my_user }}"
  tasks:
    - name: Checkout BCS_backtest code
      git: >
        repo=git@bitbucket.org:deamoon/BCS_backtest.git
        dest={{ bcs_folder }}
        accept_hostkey=yes
        key_file=quant.key
        force=yes

    - name: Creates directory
      file: path={{ item }} state=directory
      with_items:
        - "{{ bcs_folder }}/attempt"
        - "{{ bcs_folder }}/data"

    - file: >
        path={{ item }}
        owner={{ my_user }}
        group={{ my_user }}
        mode=755
        state=directory
        recurse=yes
      with_items:
        - "{{ bcs_folder }}"

    # TODO, not working
    # - local_action: stat path={{ bcs_folder }}/data/BCS_data.zip
    #   register: data_exist

    # - command: >
    #     wget -r -nH -np --cut-dirs=1 --no-check-certificate -U Mozilla --user=moy-biblio --password=biblio https://webdav.yandex.ru/BCS_data.zip
    #   args:
    #     chdir: "{{ bcs_folder }}/data"
    #   when: data_exist.stat.exists == False

    # - command: >
    #     unzip BCS_data.zip
    #   args:
    #     chdir: "{{ bcs_folder }}/data"
    #   when: data_exist.stat.exists == False

    # - name: Install Python2 requirements
    #   pip: >
    #     requirements={{ bcs_folder }}/requirements2.txt
    #     virtualenv={{ home_folder }}/.virtualenvs/{{ venv }}
    #     executable=pip2
    - name: Install Python3 requirements
      pip: >
        requirements={{ bcs_folder }}/requirements3.txt
        virtualenv={{ home_folder }}/.virtualenvs/{{ venv }}
    #    executable=pip3