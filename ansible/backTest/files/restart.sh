#!/bin/bash

sudo supervisorctl stop backtest
killall gunicorn
sudo supervisorctl start backtest

sudo supervisorctl stop celery
killall celery
sudo supervisorctl start celery
