#!/bin/bash
source {{ home_folder }}/.virtualenvs/{{ venv }}/bin/activate

HOME={{ home_folder }}

export BD_USER={{ db_user }}
export BD_PASSWORD={{ db_password }}
export BD_ALGOSITE_DATABASE_NAME={{ db_name }}
export BD_HOST=localhost

$@
