#!/bin/bash

DIR=/home/deamoon/quantforces/algosite/backup
ARCHIEVE="$(date +'%d_%m_%Y').zip"

/home/deamoon/.virtualenvs/quantforces/exec python /home/deamoon/quantforces/algosite/manage.py dumpdata --natural-foreign --natural-primary > $DIR/backup.json
/usr/bin/zip $DIR/$ARCHIEVE $DIR/backup.json
/home/deamoon/.virtualenvs/quantforces/exec python $DIR/gdrive.py $DIR/$ARCHIEVE
